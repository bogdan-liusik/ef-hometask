﻿using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu;
using ConsoleClient.Console._Menu.Enums;

namespace ConsoleClient.Console
{
    static class Program
    {
        static void Main(string[] args)
        {
            const string baseAddress = "https://localhost:5001/api/";
            using var httpClientService = new HttpClientService(baseAddress);
            
            new Menu(new LinqQueriesService(httpClientService), new CrudService(httpClientService))
                //here you can also set MenuChoise.LinqQueriesResults
                .Start(MenuChoise.CrudService);
        }
    }
}