﻿using System.Threading.Tasks;

namespace ConsoleClient.Console._Menu.MenuHandlers.Abstract
{
    public abstract class BaseHandler<TService>
    {
        private protected readonly TService Service;
            
        public BaseHandler(TService service)
        {
            Service = service;
        }

        public abstract Task Handle();
    }
}