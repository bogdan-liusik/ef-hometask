﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.User;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User
{
    using System;
    public class CreateUserHandler : BaseHandler<CrudService>
    {
        public CreateUserHandler(CrudService crudService) : base(crudService) { }

        public override async Task Handle()
        {
            try
            {
                var userCreate = new UserCreateModel();
                WriteColorLine("Enter first name: ", ConsoleColor.Yellow);
                userCreate.FirstName = Console.ReadLine();
                WriteColorLine("Enter last name: ", ConsoleColor.Yellow);
                userCreate.LastName = Console.ReadLine();
                WriteColorLine("Enter email: ", ConsoleColor.Yellow);
                userCreate.Email = Console.ReadLine();
                WriteColorLine("Enter birthday date (in format dd/mm/yyyy): ", ConsoleColor.Yellow);
                userCreate.BirthDay = DateTime.Parse(Console.ReadLine());
                
                await Service.CreateEntityAsync(userCreate, "users");
                
                WriteSuccessLine("User successfully created!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}