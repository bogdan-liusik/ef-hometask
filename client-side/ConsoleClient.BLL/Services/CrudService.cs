﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleClient.BLL.Services
{
    public class CrudService
    {
        private readonly HttpClientService _httpClientService;

        public CrudService(HttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }

        public async Task<ICollection<TEntity>> GetAllEntitiesAsync<TEntity>(string endpoint)
        {
            return await _httpClientService.FetchListObjectsAsync<TEntity>(endpoint);
        }
            
        public async Task<TEntity> GetEntityByIdAsync<TEntity>(string endpoint, int id)
        {
            return await _httpClientService.FetchObjectAsync<TEntity>($"{endpoint}/{id}");
        }

        public async Task<TEntity> CreateEntityAsync<TEntity>(TEntity entity, string endpoint)
        {
            return await _httpClientService.PostAsync(endpoint, entity);
        }
        
        public async Task<TEntity> UpdateEntityAsync<TEntity>(TEntity entity, string endpoint)
        {
            return await _httpClientService.PutAsync(entity, endpoint);
        }

        public async Task DeleteEntityAsync(int id, string endpoint)
        {
            await _httpClientService.DeleteAsync(id, endpoint);
        }
    }
}