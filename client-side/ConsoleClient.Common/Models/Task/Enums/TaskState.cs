﻿namespace ConsoleClient.Common.Models.Task.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}