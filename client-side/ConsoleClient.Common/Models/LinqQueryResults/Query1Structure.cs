﻿using ConsoleClient.Common.Models.Project;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query1Structure
    {
        public Project.Project Project { get; set; }
        public int TasksCount { get; set; }
    }
}