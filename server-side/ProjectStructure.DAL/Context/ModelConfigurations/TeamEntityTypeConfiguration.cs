﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context.ModelConfigurations
{
    public class TeamEntityTypeConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder
                .Property(t => t.Name)
                .HasMaxLength(40)
                .IsRequired();
        }
    }
}