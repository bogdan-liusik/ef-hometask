﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context.ModelConfigurations
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder
                .Property(p => p.Name)
                .HasMaxLength(70)
                .IsRequired();

            builder
                .Property(p => p.AuthorId)
                .IsRequired();
        }
    }
}