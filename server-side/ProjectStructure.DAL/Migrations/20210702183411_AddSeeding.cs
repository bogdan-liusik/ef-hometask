﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 9, 20, 17, 7, 10, 510, DateTimeKind.Unspecified).AddTicks(736), "Chadd Cruickshank" },
                    { 2, new DateTime(2019, 5, 30, 13, 19, 57, 837, DateTimeKind.Unspecified).AddTicks(5213), "Courtney Stanton" },
                    { 3, new DateTime(2019, 5, 30, 7, 43, 51, 711, DateTimeKind.Unspecified).AddTicks(9405), "Bartholome Skiles" },
                    { 4, new DateTime(2020, 10, 16, 17, 33, 27, 894, DateTimeKind.Unspecified).AddTicks(9856), "Ladarius Keebler" },
                    { 5, new DateTime(2021, 5, 7, 13, 28, 9, 562, DateTimeKind.Unspecified).AddTicks(5725), "Walter Kshlerin" },
                    { 6, new DateTime(2018, 5, 6, 22, 17, 7, 442, DateTimeKind.Unspecified).AddTicks(8856), "Marina Mosciski" },
                    { 7, new DateTime(2019, 9, 7, 7, 14, 4, 735, DateTimeKind.Unspecified).AddTicks(6992), "Deven Gibson" },
                    { 8, new DateTime(2019, 6, 19, 1, 8, 40, 283, DateTimeKind.Unspecified).AddTicks(2072), "Josue Hirthe" },
                    { 9, new DateTime(2021, 2, 10, 10, 4, 3, 905, DateTimeKind.Unspecified).AddTicks(97), "Albert Maggio" },
                    { 10, new DateTime(2018, 6, 24, 23, 22, 38, 522, DateTimeKind.Unspecified).AddTicks(5283), "Velva Prosacco" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 8, new DateTime(2010, 6, 25, 1, 37, 8, 38, DateTimeKind.Unspecified).AddTicks(962), new DateTime(2019, 3, 31, 21, 45, 54, 76, DateTimeKind.Unspecified).AddTicks(8075), "Jarrell_Rutherford26@yahoo.com", "Natalia", "Daniel", 1 },
                    { 11, new DateTime(1997, 5, 24, 12, 38, 50, 394, DateTimeKind.Unspecified).AddTicks(94), new DateTime(2015, 12, 4, 20, 49, 33, 673, DateTimeKind.Unspecified).AddTicks(3272), "Vickie48@yahoo.com", "Margarette", "Legros", 7 },
                    { 1, new DateTime(1998, 6, 20, 3, 0, 41, 679, DateTimeKind.Unspecified).AddTicks(3787), new DateTime(2012, 5, 23, 18, 5, 36, 954, DateTimeKind.Unspecified).AddTicks(3951), "Rupert41@gmail.com", "Torrey", "Rodriguez", 7 },
                    { 76, new DateTime(2012, 5, 6, 6, 21, 2, 640, DateTimeKind.Unspecified).AddTicks(7682), new DateTime(2021, 2, 19, 17, 23, 43, 932, DateTimeKind.Unspecified).AddTicks(4552), "Salma62@hotmail.com", "Rhoda", "Langworth", 6 },
                    { 68, new DateTime(1999, 4, 19, 5, 23, 37, 701, DateTimeKind.Unspecified).AddTicks(8270), new DateTime(2009, 9, 13, 15, 44, 23, 867, DateTimeKind.Unspecified).AddTicks(376), "Gerald35@gmail.com", "Ahmed", "O'Kon", 6 },
                    { 61, new DateTime(2009, 3, 21, 5, 45, 11, 838, DateTimeKind.Unspecified).AddTicks(6826), new DateTime(2018, 10, 5, 4, 45, 53, 91, DateTimeKind.Unspecified).AddTicks(9400), "Felipa_Kemmer@yahoo.com", "Lorna", "McDermott", 6 },
                    { 34, new DateTime(2000, 2, 29, 13, 34, 29, 69, DateTimeKind.Unspecified).AddTicks(8920), new DateTime(2019, 2, 7, 3, 39, 24, 402, DateTimeKind.Unspecified).AddTicks(5581), "Cleora_Schmeler2@yahoo.com", "Ashleigh", "D'Amore", 6 },
                    { 31, new DateTime(1973, 3, 4, 9, 29, 56, 570, DateTimeKind.Unspecified).AddTicks(599), new DateTime(2020, 10, 8, 6, 32, 42, 615, DateTimeKind.Unspecified).AddTicks(2465), "Frieda_Greenholt@yahoo.com", "Rebekah", "Hane", 6 },
                    { 21, new DateTime(2009, 5, 27, 20, 7, 34, 967, DateTimeKind.Unspecified).AddTicks(5630), new DateTime(2019, 12, 8, 17, 56, 44, 421, DateTimeKind.Unspecified).AddTicks(2747), "Carole.Lehner@gmail.com", "Kasandra", "Luettgen", 7 },
                    { 10, new DateTime(1997, 2, 3, 14, 2, 17, 987, DateTimeKind.Unspecified).AddTicks(6761), new DateTime(2009, 8, 27, 22, 55, 3, 270, DateTimeKind.Unspecified).AddTicks(787), "Travon.Lehner@yahoo.com", "Yadira", "Bauch", 6 },
                    { 74, new DateTime(1993, 6, 8, 3, 12, 50, 952, DateTimeKind.Unspecified).AddTicks(8045), new DateTime(2017, 11, 26, 21, 19, 21, 33, DateTimeKind.Unspecified).AddTicks(9028), "Mack.Krajcik9@hotmail.com", "Arnaldo", "Considine", 5 },
                    { 65, new DateTime(1971, 4, 1, 10, 27, 5, 945, DateTimeKind.Unspecified).AddTicks(7871), new DateTime(2000, 6, 18, 9, 14, 10, 978, DateTimeKind.Unspecified).AddTicks(2900), "Turner.Baumbach42@gmail.com", "Nichole", "Paucek", 5 },
                    { 60, new DateTime(1981, 10, 16, 21, 47, 28, 454, DateTimeKind.Unspecified).AddTicks(1430), new DateTime(2019, 1, 24, 23, 53, 15, 345, DateTimeKind.Unspecified).AddTicks(5284), "Nicholaus.Franecki@gmail.com", "Victor", "Cruickshank", 5 },
                    { 57, new DateTime(1989, 6, 30, 12, 53, 31, 797, DateTimeKind.Unspecified).AddTicks(646), new DateTime(2006, 3, 17, 23, 35, 2, 387, DateTimeKind.Unspecified).AddTicks(2451), "Cielo_Hane77@yahoo.com", "Bianka", "Schuppe", 5 },
                    { 56, new DateTime(1974, 8, 2, 13, 7, 47, 130, DateTimeKind.Unspecified).AddTicks(254), new DateTime(1991, 6, 23, 5, 29, 16, 579, DateTimeKind.Unspecified).AddTicks(5182), "Henry96@hotmail.com", "Adriana", "Morissette", 5 },
                    { 51, new DateTime(1986, 8, 23, 11, 50, 25, 411, DateTimeKind.Unspecified).AddTicks(7024), new DateTime(2001, 6, 14, 15, 27, 45, 804, DateTimeKind.Unspecified).AddTicks(9384), "Eleazar.Cassin81@gmail.com", "Eliza", "Nolan", 5 },
                    { 50, new DateTime(1998, 5, 5, 7, 37, 49, 215, DateTimeKind.Unspecified).AddTicks(9418), new DateTime(2019, 6, 23, 16, 13, 53, 951, DateTimeKind.Unspecified).AddTicks(7636), "Kayden.Jerde@gmail.com", "Ariel", "Bechtelar", 5 },
                    { 78, new DateTime(1979, 8, 3, 14, 28, 56, 648, DateTimeKind.Unspecified).AddTicks(630), new DateTime(2000, 9, 18, 12, 41, 35, 809, DateTimeKind.Unspecified).AddTicks(7486), "Palma_Gutmann56@gmail.com", "Thaddeus", "Von", 5 },
                    { 40, new DateTime(1992, 4, 11, 8, 35, 46, 468, DateTimeKind.Unspecified).AddTicks(1991), new DateTime(2015, 4, 13, 8, 26, 12, 968, DateTimeKind.Unspecified).AddTicks(8470), "Grayce.McLaughlin6@hotmail.com", "Kendrick", "Swaniawski", 5 },
                    { 28, new DateTime(1983, 3, 8, 22, 51, 33, 891, DateTimeKind.Unspecified).AddTicks(2709), new DateTime(2006, 8, 14, 9, 54, 50, 64, DateTimeKind.Unspecified).AddTicks(777), "Arlie56@yahoo.com", "Shannon", "Crona", 7 },
                    { 46, new DateTime(2001, 8, 1, 3, 53, 10, 775, DateTimeKind.Unspecified).AddTicks(5774), new DateTime(2009, 11, 29, 6, 43, 29, 33, DateTimeKind.Unspecified).AddTicks(2793), "Kristina.Dietrich56@hotmail.com", "Eugene", "Toy", 7 },
                    { 32, new DateTime(1991, 11, 27, 3, 41, 51, 641, DateTimeKind.Unspecified).AddTicks(3363), new DateTime(2015, 9, 19, 21, 29, 57, 363, DateTimeKind.Unspecified).AddTicks(3543), "Sean.Flatley@gmail.com", "Madelyn", "Hilpert", 10 },
                    { 66, new DateTime(1988, 5, 22, 3, 52, 13, 583, DateTimeKind.Unspecified).AddTicks(4464), new DateTime(2011, 9, 11, 5, 3, 29, 706, DateTimeKind.Unspecified).AddTicks(1892), "Dayna64@gmail.com", "Flo", "Stiedemann", 9 },
                    { 55, new DateTime(1973, 4, 11, 9, 22, 8, 110, DateTimeKind.Unspecified).AddTicks(8076), new DateTime(2019, 5, 20, 15, 35, 42, 484, DateTimeKind.Unspecified).AddTicks(6976), "Lorna_Robel21@yahoo.com", "Geovanni", "Marquardt", 9 },
                    { 48, new DateTime(1975, 6, 5, 15, 18, 4, 855, DateTimeKind.Unspecified).AddTicks(8091), new DateTime(1998, 9, 3, 13, 18, 36, 89, DateTimeKind.Unspecified).AddTicks(1429), "Olen_Schuster@yahoo.com", "Coty", "Wuckert", 9 },
                    { 33, new DateTime(1981, 9, 28, 6, 17, 19, 151, DateTimeKind.Unspecified).AddTicks(7490), new DateTime(2019, 3, 30, 6, 17, 36, 179, DateTimeKind.Unspecified).AddTicks(1520), "Halle49@hotmail.com", "Gianni", "Wiza", 9 },
                    { 22, new DateTime(1982, 12, 17, 3, 2, 22, 848, DateTimeKind.Unspecified).AddTicks(8554), new DateTime(2000, 5, 12, 6, 32, 26, 89, DateTimeKind.Unspecified).AddTicks(1423), "Marisol.Weimann@hotmail.com", "Santiago", "Erdman", 9 },
                    { 75, new DateTime(2011, 11, 7, 7, 28, 39, 405, DateTimeKind.Unspecified).AddTicks(7546), new DateTime(2019, 12, 17, 13, 52, 36, 607, DateTimeKind.Unspecified).AddTicks(1147), "Delores_Goyette@gmail.com", "Orion", "Stokes", 8 },
                    { 45, new DateTime(2000, 8, 1, 10, 53, 18, 712, DateTimeKind.Unspecified).AddTicks(5188), new DateTime(2020, 4, 30, 2, 27, 7, 410, DateTimeKind.Unspecified).AddTicks(1998), "Lucious_Champlin@gmail.com", "Ferne", "Jacobson", 7 },
                    { 69, new DateTime(1983, 8, 27, 22, 13, 2, 682, DateTimeKind.Unspecified).AddTicks(4743), new DateTime(2015, 8, 30, 9, 8, 44, 324, DateTimeKind.Unspecified).AddTicks(3145), "Kailey.Farrell@hotmail.com", "Janiya", "Murray", 8 },
                    { 39, new DateTime(1981, 1, 30, 9, 15, 42, 918, DateTimeKind.Unspecified).AddTicks(2075), new DateTime(2015, 7, 7, 12, 56, 35, 804, DateTimeKind.Unspecified).AddTicks(2856), "Johnathan95@yahoo.com", "Hilton", "Klein", 8 },
                    { 9, new DateTime(1979, 8, 1, 12, 50, 27, 128, DateTimeKind.Unspecified).AddTicks(5236), new DateTime(1997, 10, 8, 19, 47, 27, 495, DateTimeKind.Unspecified).AddTicks(7939), "Coleman88@yahoo.com", "Trevor", "Hammes", 8 },
                    { 5, new DateTime(1976, 1, 20, 21, 32, 12, 645, DateTimeKind.Unspecified).AddTicks(2621), new DateTime(2016, 8, 2, 11, 34, 10, 920, DateTimeKind.Unspecified).AddTicks(1385), "Sabryna_Schuppe@yahoo.com", "Wilfrid", "Quigley", 8 },
                    { 2, new DateTime(1997, 1, 30, 5, 43, 21, 391, DateTimeKind.Unspecified).AddTicks(5168), new DateTime(2010, 11, 6, 4, 11, 45, 727, DateTimeKind.Unspecified).AddTicks(6085), "Marielle85@gmail.com", "Gregg", "Pacocha", 8 },
                    { 80, new DateTime(2013, 5, 27, 17, 38, 49, 255, DateTimeKind.Unspecified).AddTicks(6700), new DateTime(2021, 6, 6, 9, 33, 14, 993, DateTimeKind.Unspecified).AddTicks(2999), "Houston.Graham@yahoo.com", "Ron", "Farrell", 7 },
                    { 71, new DateTime(1993, 7, 20, 4, 43, 56, 678, DateTimeKind.Unspecified).AddTicks(2935), new DateTime(2020, 5, 28, 2, 51, 22, 647, DateTimeKind.Unspecified).AddTicks(7455), "Isabel_King92@yahoo.com", "Tyra", "Zieme", 7 },
                    { 54, new DateTime(1983, 5, 5, 15, 52, 2, 765, DateTimeKind.Unspecified).AddTicks(7160), new DateTime(1997, 1, 19, 18, 44, 21, 635, DateTimeKind.Unspecified).AddTicks(5548), "Beulah84@yahoo.com", "Deron", "Yost", 7 },
                    { 52, new DateTime(2005, 4, 18, 15, 23, 49, 841, DateTimeKind.Unspecified).AddTicks(9042), new DateTime(2020, 4, 15, 14, 23, 37, 561, DateTimeKind.Unspecified).AddTicks(1674), "Scot.Dickinson@gmail.com", "Imelda", "Hagenes", 8 },
                    { 29, new DateTime(2001, 6, 7, 14, 44, 6, 191, DateTimeKind.Unspecified).AddTicks(1556), new DateTime(2017, 9, 15, 19, 59, 52, 832, DateTimeKind.Unspecified).AddTicks(505), "Walker_Gleichner@yahoo.com", "Aurelia", "Keeling", 5 },
                    { 20, new DateTime(2010, 7, 24, 10, 48, 19, 955, DateTimeKind.Unspecified).AddTicks(7926), new DateTime(2019, 2, 7, 11, 39, 18, 696, DateTimeKind.Unspecified).AddTicks(3871), "Nicolette46@gmail.com", "Hassie", "Block", 5 },
                    { 14, new DateTime(1997, 8, 13, 8, 40, 20, 486, DateTimeKind.Unspecified).AddTicks(8343), new DateTime(2006, 10, 12, 17, 1, 40, 554, DateTimeKind.Unspecified).AddTicks(7651), "Mckayla_Johnson@yahoo.com", "Gage", "Okuneva", 5 },
                    { 3, new DateTime(1973, 5, 4, 0, 27, 45, 12, DateTimeKind.Unspecified).AddTicks(9507), new DateTime(1988, 12, 3, 3, 15, 40, 811, DateTimeKind.Unspecified).AddTicks(1217), "Braxton_Rempel@yahoo.com", "Newton", "Kunde", 3 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 79, new DateTime(2014, 4, 22, 18, 3, 1, 972, DateTimeKind.Unspecified).AddTicks(6906), new DateTime(2022, 1, 2, 5, 35, 45, 456, DateTimeKind.Unspecified).AddTicks(9549), "Dennis_Murray10@yahoo.com", "Tania", "Carter", 2 },
                    { 59, new DateTime(1971, 12, 4, 5, 43, 53, 941, DateTimeKind.Unspecified).AddTicks(9503), new DateTime(2016, 6, 5, 0, 15, 53, 204, DateTimeKind.Unspecified).AddTicks(4739), "Dayton_Hills63@gmail.com", "Christa", "Kuhlman", 2 },
                    { 58, new DateTime(2007, 10, 11, 8, 36, 14, 407, DateTimeKind.Unspecified).AddTicks(6448), new DateTime(2017, 7, 22, 15, 2, 29, 42, DateTimeKind.Unspecified).AddTicks(7222), "Luz.Ullrich@gmail.com", "Makayla", "Deckow", 2 },
                    { 44, new DateTime(2003, 11, 9, 13, 39, 44, 123, DateTimeKind.Unspecified).AddTicks(2894), new DateTime(2013, 10, 6, 12, 15, 57, 208, DateTimeKind.Unspecified).AddTicks(574), "Van64@gmail.com", "Barton", "Dare", 2 },
                    { 42, new DateTime(1977, 11, 15, 13, 18, 19, 261, DateTimeKind.Unspecified).AddTicks(7246), new DateTime(2012, 1, 15, 16, 50, 9, 517, DateTimeKind.Unspecified).AddTicks(931), "Merlin97@gmail.com", "Hugh", "Feest", 2 },
                    { 41, new DateTime(2007, 8, 21, 7, 56, 28, 286, DateTimeKind.Unspecified).AddTicks(3366), new DateTime(2018, 6, 24, 11, 49, 17, 289, DateTimeKind.Unspecified).AddTicks(995), "Nellie59@hotmail.com", "Nora", "Herman", 2 },
                    { 4, new DateTime(1989, 5, 3, 18, 48, 42, 683, DateTimeKind.Unspecified).AddTicks(2046), new DateTime(2018, 8, 16, 4, 26, 1, 381, DateTimeKind.Unspecified).AddTicks(5057), "Donnell_OKon11@gmail.com", "Jillian", "Cormier", 3 },
                    { 25, new DateTime(1986, 7, 4, 16, 54, 57, 330, DateTimeKind.Unspecified).AddTicks(4731), new DateTime(2014, 11, 1, 15, 50, 42, 522, DateTimeKind.Unspecified).AddTicks(8879), "Cleve_Haley@yahoo.com", "Travis", "McDermott", 2 },
                    { 13, new DateTime(1994, 1, 16, 6, 15, 12, 565, DateTimeKind.Unspecified).AddTicks(7305), new DateTime(2006, 10, 16, 6, 16, 6, 665, DateTimeKind.Unspecified).AddTicks(3609), "Nils90@gmail.com", "Virginia", "Schowalter", 2 },
                    { 67, new DateTime(1988, 12, 25, 19, 57, 22, 663, DateTimeKind.Unspecified).AddTicks(8093), new DateTime(2006, 9, 12, 9, 29, 54, 383, DateTimeKind.Unspecified).AddTicks(5530), "Marcia.Lehner@yahoo.com", "Damaris", "Crona", 1 },
                    { 62, new DateTime(2012, 6, 18, 6, 4, 13, 561, DateTimeKind.Unspecified).AddTicks(3856), new DateTime(2021, 5, 28, 22, 15, 27, 776, DateTimeKind.Unspecified).AddTicks(6256), "Tracey94@gmail.com", "Deonte", "Von", 1 },
                    { 53, new DateTime(1981, 6, 2, 14, 56, 56, 412, DateTimeKind.Unspecified).AddTicks(5388), new DateTime(2003, 5, 21, 13, 9, 42, 267, DateTimeKind.Unspecified).AddTicks(1884), "Edgar_Cronin35@yahoo.com", "Kieran", "Erdman", 1 },
                    { 49, new DateTime(2003, 8, 30, 19, 10, 26, 139, DateTimeKind.Unspecified).AddTicks(6350), new DateTime(2019, 10, 12, 16, 16, 14, 32, DateTimeKind.Unspecified).AddTicks(9703), "Waino.Effertz@yahoo.com", "Noe", "Rogahn", 1 },
                    { 37, new DateTime(1991, 1, 25, 14, 17, 8, 249, DateTimeKind.Unspecified).AddTicks(5566), new DateTime(2015, 7, 26, 1, 51, 22, 30, DateTimeKind.Unspecified).AddTicks(4417), "Jameson.Kub7@gmail.com", "Madaline", "Conroy", 1 },
                    { 15, new DateTime(1993, 12, 26, 2, 10, 11, 196, DateTimeKind.Unspecified).AddTicks(18), new DateTime(2009, 10, 1, 5, 16, 58, 680, DateTimeKind.Unspecified).AddTicks(4154), "Maverick_Wolff34@yahoo.com", "Mauricio", "Gleichner", 1 },
                    { 23, new DateTime(2005, 9, 3, 23, 49, 30, 641, DateTimeKind.Unspecified).AddTicks(1982), new DateTime(2014, 11, 6, 14, 40, 52, 124, DateTimeKind.Unspecified).AddTicks(1737), "Vernon.Kulas91@hotmail.com", "Emerald", "Fritsch", 2 },
                    { 16, new DateTime(1979, 2, 10, 18, 37, 56, 379, DateTimeKind.Unspecified).AddTicks(840), new DateTime(2021, 4, 29, 6, 58, 8, 514, DateTimeKind.Unspecified).AddTicks(4784), "Kristy65@hotmail.com", "Hollie", "Turcotte", 3 },
                    { 17, new DateTime(2005, 9, 22, 18, 59, 54, 216, DateTimeKind.Unspecified).AddTicks(1938), new DateTime(2021, 5, 20, 17, 18, 41, 51, DateTimeKind.Unspecified).AddTicks(8568), "Darien.Bergnaum@yahoo.com", "Tristian", "Hilpert", 3 },
                    { 18, new DateTime(2004, 10, 11, 22, 16, 3, 786, DateTimeKind.Unspecified).AddTicks(8822), new DateTime(2021, 5, 16, 4, 58, 47, 97, DateTimeKind.Unspecified).AddTicks(1648), "Mara61@gmail.com", "Sydni", "Turcotte", 3 },
                    { 7, new DateTime(1971, 1, 21, 9, 28, 40, 714, DateTimeKind.Unspecified).AddTicks(5153), new DateTime(2004, 11, 20, 0, 12, 13, 806, DateTimeKind.Unspecified).AddTicks(5608), "Charity_Funk@gmail.com", "Etha", "Klein", 5 },
                    { 73, new DateTime(2007, 12, 3, 18, 34, 47, 466, DateTimeKind.Unspecified).AddTicks(3878), new DateTime(2016, 12, 17, 6, 23, 10, 991, DateTimeKind.Unspecified).AddTicks(175), "Ray11@yahoo.com", "Arlie", "Mueller", 4 },
                    { 47, new DateTime(1971, 12, 18, 1, 2, 0, 317, DateTimeKind.Unspecified).AddTicks(4270), new DateTime(2004, 2, 21, 23, 44, 1, 32, DateTimeKind.Unspecified).AddTicks(9937), "Abigale33@gmail.com", "Triston", "Smitham", 4 },
                    { 43, new DateTime(1999, 4, 1, 20, 40, 51, 474, DateTimeKind.Unspecified).AddTicks(7248), new DateTime(2007, 5, 8, 1, 23, 43, 384, DateTimeKind.Unspecified).AddTicks(1586), "Delphine.Batz@gmail.com", "Dana", "Waelchi", 4 },
                    { 38, new DateTime(2000, 2, 2, 3, 16, 41, 362, DateTimeKind.Unspecified).AddTicks(1736), new DateTime(2017, 9, 5, 13, 18, 24, 333, DateTimeKind.Unspecified).AddTicks(6288), "Rosario_Friesen17@yahoo.com", "Rashad", "Veum", 4 },
                    { 35, new DateTime(2009, 9, 25, 13, 16, 58, 845, DateTimeKind.Unspecified).AddTicks(3134), new DateTime(2019, 8, 12, 5, 55, 10, 260, DateTimeKind.Unspecified).AddTicks(3052), "Pearl_Waelchi@gmail.com", "Kara", "Reilly", 4 },
                    { 30, new DateTime(1987, 7, 11, 14, 42, 45, 983, DateTimeKind.Unspecified).AddTicks(247), new DateTime(2015, 9, 18, 3, 7, 44, 286, DateTimeKind.Unspecified).AddTicks(4422), "Brian.Corwin@yahoo.com", "Elbert", "Prosacco", 4 },
                    { 27, new DateTime(1991, 11, 19, 16, 54, 41, 668, DateTimeKind.Unspecified).AddTicks(4724), new DateTime(2018, 6, 6, 16, 13, 34, 502, DateTimeKind.Unspecified).AddTicks(6157), "Zoie67@yahoo.com", "Aron", "Koss", 4 },
                    { 26, new DateTime(2001, 6, 24, 14, 40, 3, 317, DateTimeKind.Unspecified).AddTicks(6516), new DateTime(2015, 5, 25, 13, 20, 32, 508, DateTimeKind.Unspecified).AddTicks(6033), "Elfrieda66@gmail.com", "Leif", "Halvorson", 4 },
                    { 12, new DateTime(2013, 5, 21, 17, 18, 19, 736, DateTimeKind.Unspecified).AddTicks(7806), new DateTime(2021, 6, 20, 13, 6, 12, 367, DateTimeKind.Unspecified).AddTicks(5397), "Dakota52@gmail.com", "Modesto", "Emmerich", 4 },
                    { 6, new DateTime(1975, 9, 2, 5, 46, 35, 527, DateTimeKind.Unspecified).AddTicks(7546), new DateTime(1998, 7, 16, 9, 14, 20, 54, DateTimeKind.Unspecified).AddTicks(1053), "Gilbert_Kiehn@hotmail.com", "Elisha", "Mitchell", 4 },
                    { 77, new DateTime(2011, 8, 11, 13, 35, 2, 698, DateTimeKind.Unspecified).AddTicks(7582), new DateTime(2020, 9, 23, 15, 20, 29, 249, DateTimeKind.Unspecified).AddTicks(1162), "Liam36@yahoo.com", "Aracely", "Heathcote", 3 },
                    { 72, new DateTime(1989, 9, 26, 5, 33, 57, 261, DateTimeKind.Unspecified).AddTicks(4601), new DateTime(1997, 9, 26, 21, 6, 21, 245, DateTimeKind.Unspecified).AddTicks(619), "Halie.McLaughlin@gmail.com", "Yvonne", "Frami", 3 },
                    { 70, new DateTime(1982, 2, 13, 9, 21, 34, 450, DateTimeKind.Unspecified).AddTicks(5530), new DateTime(2006, 3, 29, 13, 13, 49, 515, DateTimeKind.Unspecified).AddTicks(7386), "Alexys_Lemke@yahoo.com", "Dayana", "Ledner", 3 },
                    { 63, new DateTime(2009, 10, 22, 15, 49, 51, 862, DateTimeKind.Unspecified).AddTicks(4994), new DateTime(2020, 3, 30, 0, 29, 30, 139, DateTimeKind.Unspecified).AddTicks(9479), "Elenora92@yahoo.com", "Brigitte", "Huels", 3 },
                    { 24, new DateTime(1976, 9, 14, 20, 38, 27, 674, DateTimeKind.Unspecified).AddTicks(380), new DateTime(2002, 1, 5, 14, 58, 51, 970, DateTimeKind.Unspecified).AddTicks(2431), "Abdullah_King40@hotmail.com", "Sarah", "Schmidt", 3 },
                    { 19, new DateTime(1994, 8, 7, 5, 12, 30, 9, DateTimeKind.Unspecified).AddTicks(2686), new DateTime(2006, 3, 26, 8, 54, 4, 192, DateTimeKind.Unspecified).AddTicks(2672), "Blaze_Weimann12@hotmail.com", "Kathleen", "Abbott", 3 },
                    { 36, new DateTime(1982, 5, 19, 23, 45, 44, 206, DateTimeKind.Unspecified).AddTicks(7027), new DateTime(1993, 11, 25, 0, 30, 46, 846, DateTimeKind.Unspecified).AddTicks(5499), "Tierra.Stamm@hotmail.com", "Logan", "Olson", 10 },
                    { 64, new DateTime(2011, 9, 2, 10, 15, 43, 279, DateTimeKind.Unspecified).AddTicks(9220), new DateTime(2020, 2, 17, 11, 6, 38, 569, DateTimeKind.Unspecified).AddTicks(5735), "Mateo_Lockman88@yahoo.com", "Aaron", "McDermott", 10 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[,]
                {
                    { 9, 49, new DateTime(2018, 10, 24, 8, 57, 35, 796, DateTimeKind.Unspecified).AddTicks(3886), new DateTime(2018, 11, 21, 2, 17, 32, 24, DateTimeKind.Unspecified).AddTicks(6753), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Unbranded Granite Mouse", 10 },
                    { 31, 50, new DateTime(2020, 2, 19, 14, 6, 31, 624, DateTimeKind.Unspecified).AddTicks(5939), new DateTime(2020, 6, 11, 6, 10, 1, 545, DateTimeKind.Unspecified).AddTicks(8413), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Ergonomic Plastic Car", 10 },
                    { 18, 57, new DateTime(2018, 4, 6, 10, 59, 57, 914, DateTimeKind.Unspecified).AddTicks(2838), new DateTime(2019, 11, 9, 9, 39, 39, 856, DateTimeKind.Unspecified).AddTicks(1816), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Awesome Cotton Computer", 6 },
                    { 5, 60, new DateTime(2019, 1, 14, 0, 34, 13, 424, DateTimeKind.Unspecified).AddTicks(5982), new DateTime(2021, 3, 12, 17, 36, 28, 273, DateTimeKind.Unspecified).AddTicks(1722), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Rustic Soft Towels", 4 },
                    { 29, 74, new DateTime(2018, 2, 27, 1, 21, 26, 708, DateTimeKind.Unspecified).AddTicks(7175), new DateTime(2018, 9, 11, 1, 44, 21, 578, DateTimeKind.Unspecified).AddTicks(7657), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Small Plastic Shoes", 8 },
                    { 28, 78, new DateTime(2020, 9, 12, 3, 17, 21, 267, DateTimeKind.Unspecified).AddTicks(9675), new DateTime(2021, 6, 1, 17, 54, 13, 514, DateTimeKind.Unspecified).AddTicks(4314), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Generic Steel Bike", 3 },
                    { 37, 1, new DateTime(2018, 2, 19, 0, 48, 13, 665, DateTimeKind.Unspecified).AddTicks(7109), new DateTime(2021, 3, 31, 22, 54, 56, 476, DateTimeKind.Unspecified).AddTicks(2908), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Frozen Bacon", 10 },
                    { 16, 80, new DateTime(2019, 10, 25, 20, 16, 49, 413, DateTimeKind.Unspecified).AddTicks(860), new DateTime(2019, 10, 28, 19, 12, 55, 789, DateTimeKind.Unspecified).AddTicks(1430), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Refined Soft Pants", 6 },
                    { 7, 50, new DateTime(2020, 12, 25, 8, 0, 21, 590, DateTimeKind.Unspecified).AddTicks(9295), new DateTime(2021, 9, 9, 3, 32, 19, 314, DateTimeKind.Unspecified).AddTicks(4173), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Intelligent Plastic Computer", 7 },
                    { 2, 9, new DateTime(2018, 10, 12, 17, 5, 0, 128, DateTimeKind.Unspecified).AddTicks(4779), new DateTime(2021, 11, 6, 17, 42, 0, 760, DateTimeKind.Unspecified).AddTicks(7051), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Practical Steel Fish", 2 },
                    { 4, 39, new DateTime(2018, 7, 30, 16, 54, 35, 671, DateTimeKind.Unspecified).AddTicks(7861), new DateTime(2019, 5, 19, 22, 17, 24, 713, DateTimeKind.Unspecified).AddTicks(8277), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Ergonomic Frozen Shirt", 2 },
                    { 36, 75, new DateTime(2020, 11, 28, 2, 22, 10, 70, DateTimeKind.Unspecified).AddTicks(6703), new DateTime(2020, 12, 9, 15, 0, 51, 753, DateTimeKind.Unspecified).AddTicks(4084), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Handcrafted Plastic Tuna", 9 },
                    { 30, 22, new DateTime(2021, 5, 31, 9, 30, 15, 558, DateTimeKind.Unspecified).AddTicks(6663), new DateTime(2021, 8, 21, 3, 26, 32, 272, DateTimeKind.Unspecified).AddTicks(1219), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Granite Shoes", 9 },
                    { 6, 33, new DateTime(2018, 6, 7, 6, 41, 46, 313, DateTimeKind.Unspecified).AddTicks(4500), new DateTime(2019, 3, 9, 6, 57, 2, 216, DateTimeKind.Unspecified).AddTicks(3791), "The Football Is Good For Training And Recreational Purposes", "Intelligent Granite Towels", 6 },
                    { 33, 33, new DateTime(2020, 4, 4, 9, 47, 16, 373, DateTimeKind.Unspecified).AddTicks(8902), new DateTime(2021, 7, 24, 12, 53, 34, 813, DateTimeKind.Unspecified).AddTicks(5865), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Tasty Granite Bacon", 3 },
                    { 40, 48, new DateTime(2020, 12, 22, 20, 2, 27, 538, DateTimeKind.Unspecified).AddTicks(6979), new DateTime(2021, 10, 4, 17, 36, 45, 408, DateTimeKind.Unspecified).AddTicks(4393), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Sleek Soft Shirt", 10 },
                    { 24, 36, new DateTime(2019, 11, 18, 21, 38, 14, 958, DateTimeKind.Unspecified).AddTicks(1692), new DateTime(2021, 5, 30, 18, 33, 33, 428, DateTimeKind.Unspecified).AddTicks(9915), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Rubber Salad", 6 },
                    { 39, 9, new DateTime(2018, 12, 25, 16, 17, 26, 458, DateTimeKind.Unspecified).AddTicks(2552), new DateTime(2021, 8, 31, 0, 53, 55, 927, DateTimeKind.Unspecified).AddTicks(6899), "The Football Is Good For Training And Recreational Purposes", "Gorgeous Steel Car", 7 },
                    { 17, 29, new DateTime(2018, 7, 30, 15, 25, 55, 360, DateTimeKind.Unspecified).AddTicks(3552), new DateTime(2018, 8, 8, 5, 4, 21, 388, DateTimeKind.Unspecified).AddTicks(9638), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Handcrafted Soft Chips", 5 },
                    { 14, 29, new DateTime(2020, 6, 21, 14, 20, 29, 13, DateTimeKind.Unspecified).AddTicks(1111), new DateTime(2020, 11, 6, 22, 31, 17, 733, DateTimeKind.Unspecified).AddTicks(7510), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Ergonomic Steel Ball", 6 },
                    { 23, 47, new DateTime(2020, 3, 27, 13, 33, 28, 387, DateTimeKind.Unspecified).AddTicks(4664), new DateTime(2020, 9, 28, 22, 10, 12, 6, DateTimeKind.Unspecified).AddTicks(5833), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Handcrafted Frozen Chicken", 4 },
                    { 3, 67, new DateTime(2020, 1, 11, 2, 10, 15, 367, DateTimeKind.Unspecified).AddTicks(742), new DateTime(2021, 7, 5, 1, 8, 46, 703, DateTimeKind.Unspecified).AddTicks(5211), "The Football Is Good For Training And Recreational Purposes", "Fantastic Concrete Pants", 6 },
                    { 32, 25, new DateTime(2018, 11, 17, 22, 30, 0, 532, DateTimeKind.Unspecified).AddTicks(4999), new DateTime(2020, 6, 8, 3, 48, 13, 746, DateTimeKind.Unspecified).AddTicks(8676), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Generic Concrete Bacon", 10 },
                    { 12, 41, new DateTime(2021, 4, 29, 10, 39, 2, 565, DateTimeKind.Unspecified).AddTicks(5327), new DateTime(2021, 8, 23, 14, 45, 30, 195, DateTimeKind.Unspecified).AddTicks(6132), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Soft Chips", 10 },
                    { 21, 42, new DateTime(2021, 2, 28, 10, 58, 41, 326, DateTimeKind.Unspecified).AddTicks(9090), new DateTime(2021, 4, 29, 23, 54, 40, 489, DateTimeKind.Unspecified).AddTicks(1027), "The Football Is Good For Training And Recreational Purposes", "Awesome Concrete Pants", 9 },
                    { 34, 42, new DateTime(2021, 5, 1, 19, 34, 59, 793, DateTimeKind.Unspecified).AddTicks(2563), new DateTime(2021, 10, 20, 15, 16, 0, 685, DateTimeKind.Unspecified).AddTicks(9378), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Handcrafted Soft Hat", 2 },
                    { 11, 3, new DateTime(2020, 2, 26, 23, 9, 47, 692, DateTimeKind.Unspecified).AddTicks(8973), new DateTime(2020, 11, 27, 1, 47, 29, 694, DateTimeKind.Unspecified).AddTicks(7062), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Refined Concrete Tuna", 10 },
                    { 15, 3, new DateTime(2019, 1, 28, 23, 12, 2, 8, DateTimeKind.Unspecified).AddTicks(9412), new DateTime(2021, 9, 8, 12, 43, 45, 752, DateTimeKind.Unspecified).AddTicks(5881), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Handcrafted Plastic Towels", 10 },
                    { 10, 4, new DateTime(2019, 4, 29, 11, 5, 49, 511, DateTimeKind.Unspecified).AddTicks(4865), new DateTime(2020, 9, 14, 9, 2, 26, 578, DateTimeKind.Unspecified).AddTicks(7996), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Unbranded Rubber Pants", 1 },
                    { 25, 19, new DateTime(2021, 3, 5, 23, 12, 8, 908, DateTimeKind.Unspecified).AddTicks(4576), new DateTime(2021, 5, 4, 12, 50, 17, 84, DateTimeKind.Unspecified).AddTicks(9730), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Rustic Rubber Table", 6 },
                    { 35, 19, new DateTime(2021, 5, 21, 10, 15, 33, 270, DateTimeKind.Unspecified).AddTicks(6416), new DateTime(2021, 5, 24, 12, 0, 23, 439, DateTimeKind.Unspecified).AddTicks(7759), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Ergonomic Soft Pizza", 1 },
                    { 22, 70, new DateTime(2018, 9, 20, 3, 9, 9, 990, DateTimeKind.Unspecified).AddTicks(8902), new DateTime(2019, 8, 11, 14, 51, 11, 109, DateTimeKind.Unspecified).AddTicks(2210), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Rustic Granite Chair", 4 },
                    { 27, 12, new DateTime(2021, 4, 11, 23, 38, 10, 888, DateTimeKind.Unspecified).AddTicks(5223), new DateTime(2021, 6, 2, 6, 48, 7, 388, DateTimeKind.Unspecified).AddTicks(3230), "The Football Is Good For Training And Recreational Purposes", "Awesome Soft Sausages", 10 },
                    { 26, 26, new DateTime(2019, 11, 4, 22, 59, 59, 58, DateTimeKind.Unspecified).AddTicks(6965), new DateTime(2021, 1, 7, 16, 4, 59, 589, DateTimeKind.Unspecified).AddTicks(3403), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Licensed Soft Car", 9 },
                    { 1, 27, new DateTime(2019, 12, 16, 20, 13, 25, 973, DateTimeKind.Unspecified).AddTicks(7579), new DateTime(2021, 2, 27, 0, 9, 34, 616, DateTimeKind.Unspecified).AddTicks(6252), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Incredible Fresh Pizza", 3 },
                    { 13, 30, new DateTime(2020, 2, 24, 6, 0, 33, 532, DateTimeKind.Unspecified).AddTicks(5434), new DateTime(2020, 8, 1, 2, 48, 33, 961, DateTimeKind.Unspecified).AddTicks(1751), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Fantastic Concrete Tuna", 9 },
                    { 19, 30, new DateTime(2019, 9, 26, 20, 42, 44, 965, DateTimeKind.Unspecified).AddTicks(5702), new DateTime(2020, 8, 7, 1, 3, 32, 621, DateTimeKind.Unspecified).AddTicks(7287), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Ergonomic Metal Chips", 4 },
                    { 20, 38, new DateTime(2018, 11, 23, 8, 31, 25, 947, DateTimeKind.Unspecified).AddTicks(197), new DateTime(2020, 6, 15, 21, 12, 2, 278, DateTimeKind.Unspecified).AddTicks(485), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Unbranded Soft Chair", 9 },
                    { 38, 36, new DateTime(2020, 4, 14, 4, 56, 40, 765, DateTimeKind.Unspecified).AddTicks(1903), new DateTime(2021, 2, 14, 12, 18, 33, 678, DateTimeKind.Unspecified).AddTicks(4780), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Handcrafted Plastic Chips", 10 },
                    { 8, 64, new DateTime(2018, 6, 19, 6, 15, 34, 673, DateTimeKind.Unspecified).AddTicks(8092), new DateTime(2019, 8, 14, 12, 17, 36, 998, DateTimeKind.Unspecified).AddTicks(264), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Gorgeous Steel Pizza", 7 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 77, new DateTime(2018, 11, 6, 7, 39, 17, 80, DateTimeKind.Unspecified).AddTicks(3227), "Et doloribus ut tenetur possimus eaque rerum.", new DateTime(2018, 11, 18, 11, 4, 50, 135, DateTimeKind.Unspecified).AddTicks(867), "Modi cupiditate.", 52, 9, 2 },
                    { 103, new DateTime(2018, 10, 13, 13, 1, 36, 688, DateTimeKind.Unspecified).AddTicks(3593), "Voluptates adipisci incidunt rerum doloribus et ut.", new DateTime(2018, 10, 23, 15, 42, 36, 943, DateTimeKind.Unspecified).AddTicks(5192), "Recusandae consequuntur voluptatibus laborum vero.", 27, 18, 1 },
                    { 129, new DateTime(2018, 11, 2, 3, 34, 25, 5, DateTimeKind.Unspecified).AddTicks(8714), "Sed nulla reprehenderit facilis ex quibusdam quaerat et quibusdam minus.", new DateTime(2019, 6, 18, 7, 25, 57, 684, DateTimeKind.Unspecified).AddTicks(597), "Pariatur eius.", 42, 18, 0 },
                    { 43, new DateTime(2020, 6, 29, 14, 27, 42, 30, DateTimeKind.Unspecified).AddTicks(7068), "Molestias eius sint debitis aut et aut.", new DateTime(2020, 7, 6, 17, 54, 41, 46, DateTimeKind.Unspecified).AddTicks(4640), "Incidunt voluptas in officia.", 13, 5, 2 },
                    { 98, new DateTime(2020, 1, 21, 1, 14, 4, 331, DateTimeKind.Unspecified).AddTicks(7743), "Error deleniti saepe nemo voluptatum quia nisi et ratione ab sed aut officiis sit.", new DateTime(2020, 10, 11, 23, 2, 28, 941, DateTimeKind.Unspecified).AddTicks(9406), "Ut ut.", 55, 5, 0 },
                    { 2, new DateTime(2018, 8, 6, 17, 36, 49, 878, DateTimeKind.Unspecified).AddTicks(3165), "Et neque et earum corrupti sunt quia sunt labore molestiae omnis inventore.", new DateTime(2018, 8, 19, 6, 4, 4, 731, DateTimeKind.Unspecified).AddTicks(3756), "Nostrum numquam omnis recusandae tempora.", 19, 29, 2 },
                    { 31, new DateTime(2018, 7, 22, 11, 25, 18, 731, DateTimeKind.Unspecified).AddTicks(4739), "Et enim illum ex vero labore amet nemo sint quasi.", new DateTime(2018, 7, 31, 0, 58, 42, 111, DateTimeKind.Unspecified).AddTicks(5642), "Cum porro consequatur.", 10, 29, 3 },
                    { 148, new DateTime(2020, 4, 5, 11, 23, 30, 753, DateTimeKind.Unspecified).AddTicks(7568), "Eos sed perferendis laudantium qui cumque animi aut incidunt dolores voluptatem.", new DateTime(2020, 5, 8, 13, 0, 16, 331, DateTimeKind.Unspecified).AddTicks(9872), "Ut nostrum beatae necessitatibus laudantium.", 57, 31, 3 },
                    { 62, new DateTime(2018, 3, 15, 3, 36, 11, 272, DateTimeKind.Unspecified).AddTicks(1813), "Voluptatem adipisci autem expedita rerum voluptatem.", new DateTime(2018, 5, 17, 8, 22, 25, 672, DateTimeKind.Unspecified).AddTicks(7331), "Cupiditate incidunt labore explicabo quia.", 40, 29, 0 },
                    { 61, new DateTime(2021, 1, 22, 2, 15, 29, 90, DateTimeKind.Unspecified).AddTicks(6730), "Aut similique deserunt minus aliquid atque.", new DateTime(2021, 1, 22, 10, 4, 19, 231, DateTimeKind.Unspecified).AddTicks(1307), "Unde ipsum et ducimus.", 11, 28, 0 },
                    { 132, new DateTime(2020, 12, 28, 11, 1, 51, 544, DateTimeKind.Unspecified).AddTicks(1958), "Et ea enim quo et quam sunt asperiores.", new DateTime(2021, 1, 11, 16, 3, 20, 802, DateTimeKind.Unspecified).AddTicks(3332), "Et nemo.", 31, 28, 0 },
                    { 106, new DateTime(2020, 2, 19, 10, 47, 48, 265, DateTimeKind.Unspecified).AddTicks(7396), "Et eaque id necessitatibus est consequatur nostrum sint id illum porro.", new DateTime(2020, 3, 2, 8, 30, 19, 239, DateTimeKind.Unspecified).AddTicks(8589), "Sit asperiores.", 41, 37, 1 },
                    { 113, new DateTime(2020, 12, 14, 5, 25, 50, 852, DateTimeKind.Unspecified).AddTicks(2224), "Corporis et maxime quia iure maxime aspernatur tempore aut ad nobis quia.", new DateTime(2021, 3, 12, 3, 24, 26, 162, DateTimeKind.Unspecified).AddTicks(6982), "Vel enim unde nesciunt vitae.", 73, 37, 1 },
                    { 13, new DateTime(2019, 10, 25, 20, 18, 35, 140, DateTimeKind.Unspecified).AddTicks(6571), "Est voluptatibus aut facere veniam laboriosam repellat qui autem et.", new DateTime(2019, 10, 28, 2, 40, 10, 916, DateTimeKind.Unspecified).AddTicks(9907), "Eaque quia.", 54, 16, 1 },
                    { 16, new DateTime(2019, 10, 27, 15, 59, 52, 842, DateTimeKind.Unspecified).AddTicks(77), "Et error et aliquam iure ex rerum dolorum assumenda quasi.", new DateTime(2019, 10, 28, 13, 25, 22, 902, DateTimeKind.Unspecified).AddTicks(9174), "Dolores culpa sint illo praesentium.", 60, 16, 2 },
                    { 57, new DateTime(2020, 10, 20, 5, 48, 10, 51, DateTimeKind.Unspecified).AddTicks(8288), "Veniam eligendi dolor consequuntur est consequatur voluptate eligendi aut veritatis et sunt.", new DateTime(2021, 5, 5, 14, 47, 56, 55, DateTimeKind.Unspecified).AddTicks(5519), "Et eaque harum.", 33, 28, 2 },
                    { 109, new DateTime(2020, 3, 21, 8, 1, 31, 949, DateTimeKind.Unspecified).AddTicks(2548), "Rerum voluptates dignissimos ratione reiciendis quia aliquid iste qui sit.", new DateTime(2020, 5, 23, 7, 49, 40, 520, DateTimeKind.Unspecified).AddTicks(5547), "At esse.", 62, 31, 3 },
                    { 87, new DateTime(2020, 4, 9, 19, 21, 53, 830, DateTimeKind.Unspecified).AddTicks(293), "Illum repellendus dignissimos est corporis alias soluta et eaque illo.", new DateTime(2020, 5, 12, 20, 13, 4, 445, DateTimeKind.Unspecified).AddTicks(1020), "Reiciendis laboriosam molestiae voluptas.", 23, 31, 0 },
                    { 56, new DateTime(2020, 3, 8, 20, 37, 14, 412, DateTimeKind.Unspecified).AddTicks(6614), "Sequi debitis autem ut quia voluptatem veniam qui ea.", new DateTime(2020, 5, 18, 4, 38, 8, 990, DateTimeKind.Unspecified).AddTicks(9549), "Ad quia necessitatibus.", 67, 31, 3 },
                    { 140, new DateTime(2020, 6, 29, 3, 13, 4, 558, DateTimeKind.Unspecified).AddTicks(3993), "Ut enim neque veritatis voluptatem culpa et voluptate possimus alias esse iure eveniet.", new DateTime(2020, 7, 14, 11, 45, 5, 232, DateTimeKind.Unspecified).AddTicks(6583), "Itaque iusto sit cupiditate quo.", 39, 19, 1 },
                    { 139, new DateTime(2020, 1, 1, 4, 14, 14, 146, DateTimeKind.Unspecified).AddTicks(5476), "Dignissimos sed ipsam totam ab consectetur in vitae ut.", new DateTime(2020, 2, 27, 5, 25, 43, 711, DateTimeKind.Unspecified).AddTicks(9898), "Et sunt dignissimos similique.", 66, 20, 0 },
                    { 3, new DateTime(2020, 8, 24, 13, 54, 49, 880, DateTimeKind.Unspecified).AddTicks(9781), "Est qui accusamus saepe qui accusamus.", new DateTime(2020, 9, 22, 13, 12, 37, 50, DateTimeKind.Unspecified).AddTicks(6101), "Laudantium praesentium rem hic.", 12, 23, 1 },
                    { 10, new DateTime(2020, 9, 8, 20, 40, 56, 943, DateTimeKind.Unspecified).AddTicks(4851), "Non tenetur iusto sunt qui sunt pariatur quae expedita consequatur dolorem magnam eveniet nisi.", new DateTime(2020, 9, 20, 20, 23, 7, 839, DateTimeKind.Unspecified).AddTicks(9764), "Quidem alias porro fuga accusamus.", 30, 23, 1 },
                    { 42, new DateTime(2020, 9, 14, 18, 35, 56, 461, DateTimeKind.Unspecified).AddTicks(1286), "Eum minima nisi veniam porro aliquid ipsa.", new DateTime(2020, 9, 21, 5, 10, 36, 550, DateTimeKind.Unspecified).AddTicks(884), "Amet ea.", 20, 23, 2 },
                    { 100, new DateTime(2020, 4, 30, 16, 21, 31, 741, DateTimeKind.Unspecified).AddTicks(4978), "Tempore optio ipsum expedita sequi quo tempore non eius harum vel qui tenetur.", new DateTime(2020, 9, 8, 16, 6, 11, 568, DateTimeKind.Unspecified).AddTicks(5889), "Voluptatem molestiae quia quia.", 26, 23, 0 },
                    { 128, new DateTime(2020, 4, 25, 19, 56, 47, 834, DateTimeKind.Unspecified).AddTicks(5660), "Id nobis amet unde quo iusto accusamus quia aliquid ipsa velit impedit.", new DateTime(2020, 6, 9, 4, 7, 9, 533, DateTimeKind.Unspecified).AddTicks(4646), "Quos qui.", 24, 23, 2 },
                    { 51, new DateTime(2020, 7, 17, 0, 59, 53, 550, DateTimeKind.Unspecified).AddTicks(3250), "Molestiae consequatur est itaque est dignissimos pariatur.", new DateTime(2020, 9, 18, 0, 41, 12, 289, DateTimeKind.Unspecified).AddTicks(4030), "Est esse dolorem ratione eos.", 71, 14, 3 },
                    { 150, new DateTime(2020, 6, 29, 9, 11, 1, 743, DateTimeKind.Unspecified).AddTicks(5274), "Laudantium provident magni sit voluptate soluta velit in.", new DateTime(2020, 8, 29, 8, 37, 52, 341, DateTimeKind.Unspecified).AddTicks(4457), "Molestiae minima magni.", 80, 14, 3 },
                    { 47, new DateTime(2018, 7, 31, 11, 39, 29, 952, DateTimeKind.Unspecified).AddTicks(7678), "Cupiditate et natus tenetur optio sint sint.", new DateTime(2018, 8, 1, 22, 18, 57, 999, DateTimeKind.Unspecified).AddTicks(2919), "Ex iste et autem nam.", 6, 17, 1 },
                    { 120, new DateTime(2018, 8, 5, 0, 31, 49, 178, DateTimeKind.Unspecified).AddTicks(8202), "Officiis consectetur numquam tempore eos animi voluptas illo ex aspernatur autem.", new DateTime(2018, 8, 7, 5, 39, 46, 944, DateTimeKind.Unspecified).AddTicks(302), "Sapiente voluptatibus animi voluptatibus.", 34, 17, 3 },
                    { 35, new DateTime(2021, 8, 26, 0, 7, 50, 613, DateTimeKind.Unspecified).AddTicks(7332), "Consequatur voluptas alias quia aut vitae aut id et.", new DateTime(2021, 8, 26, 12, 37, 17, 291, DateTimeKind.Unspecified).AddTicks(2858), "Ex magnam dolores.", 46, 7, 3 },
                    { 66, new DateTime(2021, 1, 6, 13, 32, 1, 645, DateTimeKind.Unspecified).AddTicks(318), "Quo inventore omnis nobis amet consequatur est tenetur at recusandae quis.", new DateTime(2021, 6, 9, 16, 26, 3, 919, DateTimeKind.Unspecified).AddTicks(5657), "Repellat sapiente laboriosam minus.", 35, 7, 1 },
                    { 84, new DateTime(2021, 4, 21, 19, 38, 6, 187, DateTimeKind.Unspecified).AddTicks(3422), "Enim deserunt modi quia sit assumenda illo.", new DateTime(2021, 8, 21, 21, 3, 3, 754, DateTimeKind.Unspecified).AddTicks(8363), "Voluptatem voluptatem rerum id blanditiis.", 21, 7, 3 },
                    { 46, new DateTime(2020, 5, 16, 7, 18, 49, 94, DateTimeKind.Unspecified).AddTicks(4384), "Qui accusantium saepe nesciunt pariatur provident fugit aperiam.", new DateTime(2020, 5, 21, 9, 58, 46, 374, DateTimeKind.Unspecified).AddTicks(7477), "Ut dolorem et hic ducimus.", 29, 31, 2 },
                    { 17, new DateTime(2019, 10, 26, 23, 53, 36, 224, DateTimeKind.Unspecified).AddTicks(1966), "Sequi odio voluptatum exercitationem laboriosam ut itaque ut beatae omnis architecto.", new DateTime(2019, 10, 27, 9, 33, 42, 118, DateTimeKind.Unspecified).AddTicks(7590), "Nostrum quasi.", 34, 16, 1 },
                    { 146, new DateTime(2019, 10, 27, 4, 20, 17, 479, DateTimeKind.Unspecified).AddTicks(2456), "Maxime et voluptatem aut aut fugiat similique temporibus quos soluta.", new DateTime(2019, 10, 27, 11, 51, 48, 948, DateTimeKind.Unspecified).AddTicks(3684), "Perspiciatis commodi sint veritatis in.", 29, 16, 2 },
                    { 97, new DateTime(2019, 7, 25, 7, 50, 9, 630, DateTimeKind.Unspecified).AddTicks(2150), "Odio enim consectetur ut vero non quibusdam.", new DateTime(2021, 2, 11, 22, 48, 24, 733, DateTimeKind.Unspecified).AddTicks(3256), "Debitis ex ad dicta.", 12, 2, 1 },
                    { 39, new DateTime(2019, 4, 5, 22, 5, 30, 335, DateTimeKind.Unspecified).AddTicks(2593), "Quam consequatur ut quas officiis ratione aliquid quia dignissimos doloribus.", new DateTime(2021, 7, 5, 18, 29, 41, 702, DateTimeKind.Unspecified).AddTicks(9103), "Nemo amet labore quam aut.", 30, 39, 1 },
                    { 60, new DateTime(2021, 7, 31, 5, 9, 35, 512, DateTimeKind.Unspecified).AddTicks(1655), "Qui voluptatum ut est porro perspiciatis odit magnam.", new DateTime(2021, 8, 12, 4, 50, 3, 287, DateTimeKind.Unspecified).AddTicks(1461), "Eos cumque magnam ducimus.", 79, 40, 3 },
                    { 78, new DateTime(2021, 7, 20, 5, 12, 2, 890, DateTimeKind.Unspecified).AddTicks(1068), "Omnis hic numquam vero dolore eius aut inventore quasi.", new DateTime(2021, 9, 14, 10, 1, 56, 850, DateTimeKind.Unspecified).AddTicks(5082), "Itaque quo.", 21, 40, 0 },
                    { 81, new DateTime(2021, 7, 14, 13, 56, 43, 614, DateTimeKind.Unspecified).AddTicks(3837), "Adipisci est id minus assumenda cum perspiciatis molestiae.", new DateTime(2021, 9, 6, 4, 22, 56, 163, DateTimeKind.Unspecified).AddTicks(748), "Ipsum repudiandae consequatur laudantium corporis.", 19, 40, 2 },
                    { 134, new DateTime(2021, 4, 15, 1, 9, 5, 751, DateTimeKind.Unspecified).AddTicks(1030), "Voluptatem labore totam dolorem ea odit cum delectus et similique est dignissimos fugit.", new DateTime(2021, 9, 1, 15, 2, 49, 351, DateTimeKind.Unspecified).AddTicks(1260), "Quae velit.", 36, 40, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 136, new DateTime(2021, 7, 19, 15, 26, 18, 549, DateTimeKind.Unspecified).AddTicks(4359), "Labore ut accusamus aut officia beatae eum commodi iusto maxime sit deserunt pariatur.", new DateTime(2021, 8, 20, 1, 30, 55, 731, DateTimeKind.Unspecified).AddTicks(2624), "Mollitia earum nihil quis sed.", 52, 40, 1 },
                    { 143, new DateTime(2021, 9, 26, 18, 39, 12, 307, DateTimeKind.Unspecified).AddTicks(8004), "Possimus dolores provident molestiae ex iusto est.", new DateTime(2021, 10, 2, 1, 56, 7, 323, DateTimeKind.Unspecified).AddTicks(4071), "Occaecati qui et.", 48, 40, 1 },
                    { 44, new DateTime(2020, 11, 10, 7, 16, 38, 504, DateTimeKind.Unspecified).AddTicks(2760), "Aut quaerat veritatis necessitatibus omnis pariatur vitae ad magnam.", new DateTime(2021, 1, 5, 1, 49, 16, 669, DateTimeKind.Unspecified).AddTicks(5349), "Debitis quas illo nemo eos.", 65, 24, 2 },
                    { 69, new DateTime(2020, 5, 9, 9, 37, 46, 305, DateTimeKind.Unspecified).AddTicks(8344), "Corrupti tenetur deserunt ullam quia eligendi veniam.", new DateTime(2020, 9, 2, 3, 9, 0, 835, DateTimeKind.Unspecified).AddTicks(5012), "Nesciunt delectus.", 31, 24, 0 },
                    { 75, new DateTime(2020, 11, 18, 15, 53, 55, 881, DateTimeKind.Unspecified).AddTicks(49), "Saepe quam autem harum officia nostrum eius qui illo.", new DateTime(2020, 12, 20, 21, 6, 37, 923, DateTimeKind.Unspecified).AddTicks(8358), "Id iusto est exercitationem.", 37, 24, 0 },
                    { 112, new DateTime(2020, 4, 22, 18, 9, 5, 936, DateTimeKind.Unspecified).AddTicks(2897), "Laborum cupiditate fuga eligendi cumque neque sed quod eum.", new DateTime(2021, 5, 11, 23, 39, 4, 194, DateTimeKind.Unspecified).AddTicks(8219), "Excepturi aut laborum.", 61, 24, 1 },
                    { 104, new DateTime(2020, 10, 11, 9, 21, 3, 68, DateTimeKind.Unspecified).AddTicks(7921), "Voluptates consequatur rerum ea nobis vel consequatur quasi et.", new DateTime(2020, 10, 28, 10, 41, 30, 355, DateTimeKind.Unspecified).AddTicks(933), "Facere numquam exercitationem possimus impedit.", 52, 38, 1 },
                    { 105, new DateTime(2020, 4, 20, 15, 49, 38, 933, DateTimeKind.Unspecified).AddTicks(894), "Totam amet neque vero saepe aut cupiditate beatae exercitationem id quidem consequatur enim tempora.", new DateTime(2020, 12, 14, 20, 39, 14, 942, DateTimeKind.Unspecified).AddTicks(1207), "Praesentium exercitationem eligendi earum.", 44, 38, 2 },
                    { 149, new DateTime(2020, 7, 31, 17, 40, 59, 632, DateTimeKind.Unspecified).AddTicks(7187), "Magnam magni ipsum expedita delectus consectetur sed non et voluptas.", new DateTime(2021, 1, 14, 10, 44, 24, 951, DateTimeKind.Unspecified).AddTicks(3399), "Laborum saepe.", 38, 38, 0 },
                    { 89, new DateTime(2018, 8, 9, 16, 26, 52, 287, DateTimeKind.Unspecified).AddTicks(121), "Velit quas consequatur magnam doloremque provident enim magni blanditiis dolore.", new DateTime(2019, 7, 1, 22, 46, 0, 880, DateTimeKind.Unspecified).AddTicks(5592), "Sequi optio mollitia neque laborum.", 73, 8, 2 },
                    { 108, new DateTime(2018, 12, 12, 13, 2, 52, 250, DateTimeKind.Unspecified).AddTicks(1864), "Doloremque omnis omnis nemo tempora voluptas doloribus quis officiis repellendus qui eius optio.", new DateTime(2019, 8, 9, 20, 42, 5, 293, DateTimeKind.Unspecified).AddTicks(2484), "Nesciunt sed quo quo alias.", 80, 8, 1 },
                    { 33, new DateTime(2021, 5, 6, 4, 35, 13, 712, DateTimeKind.Unspecified).AddTicks(2922), "Beatae et voluptas sapiente et harum corrupti perspiciatis omnis non consequatur.", new DateTime(2021, 9, 7, 3, 2, 15, 81, DateTimeKind.Unspecified).AddTicks(4437), "Mollitia amet et atque enim.", 80, 40, 1 },
                    { 130, new DateTime(2020, 2, 26, 21, 59, 59, 656, DateTimeKind.Unspecified).AddTicks(6883), "Et vel ad doloribus quae similique.", new DateTime(2020, 7, 21, 3, 20, 53, 949, DateTimeKind.Unspecified).AddTicks(8520), "Ipsam ducimus voluptatem ex.", 52, 19, 3 },
                    { 30, new DateTime(2021, 8, 4, 4, 37, 58, 860, DateTimeKind.Unspecified).AddTicks(2760), "Libero tempore culpa quia ratione quis.", new DateTime(2021, 9, 19, 10, 9, 22, 742, DateTimeKind.Unspecified).AddTicks(4533), "Ea et id hic.", 7, 40, 1 },
                    { 23, new DateTime(2021, 5, 12, 6, 46, 52, 840, DateTimeKind.Unspecified).AddTicks(4716), "Sit voluptatibus autem numquam doloremque nihil voluptatem inventore repudiandae natus.", new DateTime(2021, 9, 17, 9, 33, 17, 283, DateTimeKind.Unspecified).AddTicks(2353), "Voluptatibus aperiam.", 75, 40, 2 },
                    { 79, new DateTime(2019, 5, 24, 21, 38, 46, 879, DateTimeKind.Unspecified).AddTicks(2905), "Impedit quisquam quasi ipsa rerum iusto possimus quas facere debitis magnam laboriosam alias.", new DateTime(2019, 5, 30, 11, 50, 37, 825, DateTimeKind.Unspecified).AddTicks(9123), "Iusto facilis ad error.", 54, 39, 1 },
                    { 118, new DateTime(2021, 4, 29, 8, 18, 24, 441, DateTimeKind.Unspecified).AddTicks(1269), "Maiores et itaque et aut accusantium iste error atque labore quia.", new DateTime(2021, 8, 20, 13, 17, 1, 278, DateTimeKind.Unspecified).AddTicks(8334), "Assumenda consequatur ut neque consequatur.", 14, 39, 1 },
                    { 144, new DateTime(2018, 8, 7, 21, 51, 31, 982, DateTimeKind.Unspecified).AddTicks(7950), "Eligendi officia qui voluptatibus totam pariatur fugiat eligendi modi quo est explicabo fugiat voluptatem.", new DateTime(2019, 3, 20, 21, 17, 19, 789, DateTimeKind.Unspecified).AddTicks(2691), "Eius impedit ipsum quam eligendi.", 2, 4, 1 },
                    { 20, new DateTime(2020, 12, 6, 1, 28, 21, 210, DateTimeKind.Unspecified).AddTicks(6706), "Officia voluptates et repellendus repellendus provident magnam veniam eius laudantium tenetur natus cupiditate.", new DateTime(2020, 12, 6, 4, 53, 5, 164, DateTimeKind.Unspecified).AddTicks(8492), "Natus quos incidunt numquam.", 24, 36, 1 },
                    { 52, new DateTime(2020, 12, 7, 16, 28, 25, 907, DateTimeKind.Unspecified).AddTicks(7374), "Aut cum ducimus praesentium ratione minima neque enim quo hic.", new DateTime(2020, 12, 9, 0, 50, 54, 751, DateTimeKind.Unspecified).AddTicks(9536), "Adipisci et molestiae accusantium sed.", 16, 36, 2 },
                    { 82, new DateTime(2020, 12, 5, 7, 41, 55, 127, DateTimeKind.Unspecified).AddTicks(2133), "Commodi debitis praesentium sapiente consequuntur ipsam sed enim quo facere magni eum.", new DateTime(2020, 12, 5, 13, 22, 25, 481, DateTimeKind.Unspecified).AddTicks(6087), "Quis hic ipsa a similique.", 40, 36, 3 },
                    { 117, new DateTime(2020, 11, 29, 19, 43, 29, 663, DateTimeKind.Unspecified).AddTicks(8308), "Sapiente sit architecto animi vel quidem harum error maiores sed dolorem laudantium.", new DateTime(2020, 12, 7, 11, 1, 22, 628, DateTimeKind.Unspecified).AddTicks(1581), "Consequatur sint similique culpa molestiae.", 44, 36, 3 },
                    { 55, new DateTime(2021, 8, 16, 6, 0, 34, 156, DateTimeKind.Unspecified).AddTicks(1152), "Tempora molestias aperiam minima provident tempore ipsa sunt omnis suscipit a et mollitia.", new DateTime(2021, 8, 17, 10, 45, 3, 955, DateTimeKind.Unspecified).AddTicks(711), "Aliquam beatae eaque commodi.", 2, 30, 0 },
                    { 99, new DateTime(2021, 8, 10, 5, 57, 15, 539, DateTimeKind.Unspecified).AddTicks(9931), "Quas nostrum aliquam doloremque non dolore ad porro ea.", new DateTime(2021, 8, 14, 9, 42, 56, 90, DateTimeKind.Unspecified).AddTicks(8815), "Mollitia maxime non.", 63, 30, 2 },
                    { 15, new DateTime(2018, 10, 24, 23, 3, 4, 891, DateTimeKind.Unspecified).AddTicks(2010), "Et exercitationem omnis ab libero pariatur repellendus veritatis magni quod eos ducimus quas facere.", new DateTime(2018, 12, 1, 19, 31, 19, 557, DateTimeKind.Unspecified).AddTicks(7752), "Numquam aliquid fugiat.", 10, 6, 1 },
                    { 54, new DateTime(2018, 9, 16, 5, 51, 14, 696, DateTimeKind.Unspecified).AddTicks(7022), "Magnam et est amet quo cum aut fugiat doloremque.", new DateTime(2018, 12, 20, 17, 48, 8, 12, DateTimeKind.Unspecified).AddTicks(4324), "Distinctio dignissimos magni aut.", 25, 6, 0 },
                    { 65, new DateTime(2019, 1, 22, 19, 51, 13, 552, DateTimeKind.Unspecified).AddTicks(8103), "Ipsum ipsum illo reprehenderit neque veniam id.", new DateTime(2019, 2, 4, 17, 39, 8, 253, DateTimeKind.Unspecified).AddTicks(6900), "Culpa a vel dolores.", 23, 6, 1 },
                    { 24, new DateTime(2021, 5, 1, 14, 34, 39, 328, DateTimeKind.Unspecified).AddTicks(148), "Voluptatem sequi ad alias facere consequatur maiores quo et expedita et et repellendus est.", new DateTime(2021, 6, 24, 3, 25, 32, 791, DateTimeKind.Unspecified).AddTicks(6260), "Sit est maiores.", 59, 33, 1 },
                    { 28, new DateTime(2020, 10, 22, 0, 45, 57, 909, DateTimeKind.Unspecified).AddTicks(7350), "Amet quo eos repellat qui autem expedita ullam.", new DateTime(2021, 3, 28, 16, 25, 6, 894, DateTimeKind.Unspecified).AddTicks(1546), "Nulla sunt ipsam dolorum laudantium.", 79, 33, 1 },
                    { 4, new DateTime(2021, 5, 6, 3, 18, 44, 900, DateTimeKind.Unspecified).AddTicks(4036), "Non quis distinctio accusamus sed et eos suscipit dicta itaque.", new DateTime(2021, 8, 30, 17, 7, 51, 29, DateTimeKind.Unspecified).AddTicks(8227), "Molestiae minima fuga incidunt.", 24, 40, 0 },
                    { 25, new DateTime(2021, 2, 16, 11, 33, 17, 505, DateTimeKind.Unspecified).AddTicks(8192), "Sed nobis pariatur id minus qui.", new DateTime(2021, 9, 2, 20, 40, 11, 949, DateTimeKind.Unspecified).AddTicks(5129), "Quibusdam necessitatibus tempore modi molestiae.", 57, 40, 3 },
                    { 119, new DateTime(2020, 1, 3, 20, 41, 7, 956, DateTimeKind.Unspecified).AddTicks(4578), "Ab tempora id officia ut autem vel sint non et quis.", new DateTime(2020, 6, 2, 23, 4, 52, 380, DateTimeKind.Unspecified).AddTicks(5498), "Dolor aut impedit et.", 17, 19, 1 },
                    { 101, new DateTime(2020, 5, 9, 23, 38, 21, 635, DateTimeKind.Unspecified).AddTicks(5350), "Nihil modi qui eligendi sint commodi esse.", new DateTime(2020, 7, 8, 7, 56, 21, 535, DateTimeKind.Unspecified).AddTicks(8172), "Ut est qui optio.", 65, 19, 1 },
                    { 37, new DateTime(2020, 2, 2, 2, 13, 7, 288, DateTimeKind.Unspecified).AddTicks(7994), "Voluptate optio deleniti similique molestias nemo officia aut nemo est velit ab.", new DateTime(2020, 3, 6, 21, 25, 47, 677, DateTimeKind.Unspecified).AddTicks(5049), "Illo dolorem enim excepturi excepturi.", 55, 19, 0 },
                    { 74, new DateTime(2021, 7, 31, 15, 44, 28, 927, DateTimeKind.Unspecified).AddTicks(8066), "Est debitis sint ut beatae corrupti dolor ipsum perspiciatis quae aliquam sunt ut eaque.", new DateTime(2021, 8, 12, 16, 45, 53, 913, DateTimeKind.Unspecified).AddTicks(5388), "Voluptas nulla magnam.", 46, 12, 2 },
                    { 27, new DateTime(2021, 3, 21, 21, 0, 29, 188, DateTimeKind.Unspecified).AddTicks(8985), "Ullam et dolorem molestiae distinctio voluptates libero dolores tenetur eum ducimus quia.", new DateTime(2021, 4, 19, 16, 53, 9, 786, DateTimeKind.Unspecified).AddTicks(1218), "Ab architecto expedita.", 34, 21, 3 },
                    { 92, new DateTime(2021, 3, 26, 15, 31, 55, 524, DateTimeKind.Unspecified).AddTicks(1494), "Quae quos pariatur vel quasi rerum dolor quae.", new DateTime(2021, 4, 4, 23, 24, 1, 581, DateTimeKind.Unspecified).AddTicks(5441), "Consequatur inventore quaerat.", 13, 21, 0 },
                    { 124, new DateTime(2021, 3, 10, 16, 33, 30, 944, DateTimeKind.Unspecified).AddTicks(4643), "Et ullam atque aut et recusandae.", new DateTime(2021, 4, 3, 10, 3, 23, 878, DateTimeKind.Unspecified).AddTicks(6313), "Aut labore vitae quia.", 1, 21, 0 },
                    { 147, new DateTime(2021, 3, 26, 13, 5, 41, 114, DateTimeKind.Unspecified).AddTicks(7551), "Suscipit consequatur odio quod enim earum veritatis ut commodi.", new DateTime(2021, 4, 12, 10, 42, 14, 238, DateTimeKind.Unspecified).AddTicks(636), "Magni sit maxime qui.", 80, 21, 1 },
                    { 110, new DateTime(2021, 5, 27, 5, 1, 19, 252, DateTimeKind.Unspecified).AddTicks(451), "Dicta ea aliquam officiis totam odit id eos.", new DateTime(2021, 7, 25, 12, 21, 33, 165, DateTimeKind.Unspecified).AddTicks(5956), "Praesentium vel ducimus numquam.", 46, 34, 2 },
                    { 122, new DateTime(2021, 10, 8, 9, 54, 47, 942, DateTimeKind.Unspecified).AddTicks(3413), "Rem dignissimos ad beatae adipisci corporis doloribus molestias consequuntur voluptates eius.", new DateTime(2021, 10, 12, 16, 16, 41, 640, DateTimeKind.Unspecified).AddTicks(1948), "Ullam recusandae non.", 48, 34, 2 },
                    { 131, new DateTime(2021, 7, 15, 1, 8, 16, 117, DateTimeKind.Unspecified).AddTicks(6717), "Ex rerum laborum dolorum omnis nemo delectus dolor adipisci libero facilis cum reiciendis dolor.", new DateTime(2021, 8, 14, 22, 0, 36, 802, DateTimeKind.Unspecified).AddTicks(9703), "Enim dolorum quo autem.", 38, 34, 0 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 34, new DateTime(2020, 9, 22, 5, 30, 23, 761, DateTimeKind.Unspecified).AddTicks(6268), "Aliquid omnis qui tempore error tempore fugit itaque possimus sequi.", new DateTime(2020, 10, 15, 3, 13, 24, 959, DateTimeKind.Unspecified).AddTicks(7449), "Voluptatem corporis quia dolorum.", 42, 11, 1 },
                    { 70, new DateTime(2020, 5, 11, 17, 37, 4, 59, DateTimeKind.Unspecified).AddTicks(3948), "Molestiae temporibus adipisci ipsam modi nihil.", new DateTime(2020, 9, 23, 3, 16, 37, 295, DateTimeKind.Unspecified).AddTicks(1425), "Aperiam mollitia.", 4, 11, 2 },
                    { 86, new DateTime(2020, 9, 5, 16, 52, 42, 465, DateTimeKind.Unspecified).AddTicks(8381), "Illo ea voluptas alias mollitia voluptas fugit sed laudantium.", new DateTime(2020, 10, 21, 15, 18, 48, 376, DateTimeKind.Unspecified).AddTicks(9079), "Consequatur quibusdam magnam sit qui.", 65, 11, 2 },
                    { 137, new DateTime(2020, 6, 30, 8, 19, 10, 426, DateTimeKind.Unspecified).AddTicks(2202), "Libero mollitia quo odit dolores ullam eos corporis laborum ut ea qui laboriosam.", new DateTime(2020, 11, 11, 3, 11, 30, 665, DateTimeKind.Unspecified).AddTicks(2721), "Amet autem inventore.", 9, 11, 2 },
                    { 32, new DateTime(2019, 7, 2, 7, 16, 43, 824, DateTimeKind.Unspecified).AddTicks(8154), "Eum voluptas repellat consequatur ullam aut rerum maiores saepe iste.", new DateTime(2020, 8, 2, 19, 52, 10, 366, DateTimeKind.Unspecified).AddTicks(7339), "Impedit quia amet quidem.", 45, 15, 2 },
                    { 49, new DateTime(2020, 4, 8, 15, 26, 38, 709, DateTimeKind.Unspecified).AddTicks(6818), "Ullam rerum neque nihil delectus unde quo et ratione quod praesentium animi.", new DateTime(2021, 2, 22, 18, 59, 36, 933, DateTimeKind.Unspecified).AddTicks(269), "Totam earum laudantium sit et.", 48, 15, 3 },
                    { 67, new DateTime(2021, 4, 14, 12, 34, 34, 553, DateTimeKind.Unspecified).AddTicks(8184), "Voluptate placeat saepe enim qui mollitia excepturi quia voluptatem similique.", new DateTime(2021, 6, 11, 7, 9, 54, 243, DateTimeKind.Unspecified).AddTicks(9291), "Beatae ipsum.", 54, 15, 2 },
                    { 45, new DateTime(2021, 7, 5, 1, 2, 41, 890, DateTimeKind.Unspecified).AddTicks(2198), "Hic corporis repudiandae sit mollitia voluptates commodi sed.", new DateTime(2021, 8, 10, 8, 55, 45, 793, DateTimeKind.Unspecified).AddTicks(7763), "Soluta qui.", 54, 12, 3 },
                    { 123, new DateTime(2019, 3, 20, 17, 14, 49, 993, DateTimeKind.Unspecified).AddTicks(8596), "Nulla molestiae excepturi quis minus eum est.", new DateTime(2021, 1, 28, 17, 36, 50, 356, DateTimeKind.Unspecified).AddTicks(478), "Voluptate quaerat.", 67, 15, 3 },
                    { 38, new DateTime(2021, 5, 28, 3, 33, 4, 232, DateTimeKind.Unspecified).AddTicks(1322), "Harum qui ea aliquam consequuntur quia voluptas ab temporibus.", new DateTime(2021, 7, 18, 22, 0, 33, 1, DateTimeKind.Unspecified).AddTicks(2820), "Iure et repellendus ducimus quia.", 20, 12, 0 },
                    { 85, new DateTime(2019, 10, 5, 15, 47, 41, 563, DateTimeKind.Unspecified).AddTicks(9345), "Consequatur corporis ut eveniet sit soluta laborum aut minima corrupti exercitationem ut consequatur.", new DateTime(2019, 12, 6, 15, 40, 48, 29, DateTimeKind.Unspecified).AddTicks(7200), "Rem ea.", 30, 32, 0 },
                    { 83, new DateTime(2018, 11, 16, 2, 52, 59, 214, DateTimeKind.Unspecified).AddTicks(3853), "A voluptatem blanditiis distinctio modi quam sed deserunt.", new DateTime(2018, 11, 20, 13, 52, 36, 128, DateTimeKind.Unspecified).AddTicks(9783), "Nihil excepturi.", 47, 9, 0 },
                    { 94, new DateTime(2018, 11, 11, 23, 17, 26, 795, DateTimeKind.Unspecified).AddTicks(9126), "Qui repudiandae culpa ut quas asperiores consequuntur aspernatur iusto facilis a atque blanditiis.", new DateTime(2018, 11, 11, 23, 40, 45, 336, DateTimeKind.Unspecified).AddTicks(8454), "Aliquam eum et molestias dolore.", 40, 9, 1 },
                    { 96, new DateTime(2018, 11, 5, 4, 1, 34, 759, DateTimeKind.Unspecified).AddTicks(7019), "Blanditiis et natus eum quia est repellendus aut eum unde corporis sit non veniam.", new DateTime(2018, 11, 15, 0, 8, 52, 608, DateTimeKind.Unspecified).AddTicks(9375), "Corrupti sequi deleniti mollitia.", 31, 9, 2 },
                    { 102, new DateTime(2018, 11, 16, 3, 20, 26, 487, DateTimeKind.Unspecified).AddTicks(1875), "Sunt maxime dolores est consequatur quos molestiae eum voluptatibus provident qui est qui.", new DateTime(2018, 11, 16, 3, 25, 11, 826, DateTimeKind.Unspecified).AddTicks(1558), "Animi officiis.", 4, 9, 3 },
                    { 145, new DateTime(2018, 11, 16, 14, 7, 15, 612, DateTimeKind.Unspecified).AddTicks(1317), "Voluptatem sapiente est doloremque reprehenderit ex.", new DateTime(2018, 11, 20, 7, 13, 32, 434, DateTimeKind.Unspecified).AddTicks(7125), "Minus suscipit.", 72, 9, 2 },
                    { 6, new DateTime(2020, 1, 29, 14, 54, 23, 173, DateTimeKind.Unspecified).AddTicks(49), "Iste enim recusandae qui impedit nisi.", new DateTime(2021, 3, 22, 5, 31, 58, 413, DateTimeKind.Unspecified).AddTicks(917), "Dolorem fugit quisquam.", 5, 3, 0 },
                    { 21, new DateTime(2020, 4, 6, 3, 27, 27, 192, DateTimeKind.Unspecified).AddTicks(1617), "Atque repellat officiis consequuntur rerum porro dignissimos laudantium eaque consequatur ut et non quod.", new DateTime(2020, 10, 15, 10, 50, 49, 334, DateTimeKind.Unspecified).AddTicks(9355), "Qui aliquam rerum.", 45, 3, 0 },
                    { 29, new DateTime(2020, 5, 14, 13, 25, 50, 168, DateTimeKind.Unspecified).AddTicks(3915), "Odit iste doloribus quis ex consequatur omnis quidem.", new DateTime(2020, 6, 18, 17, 57, 23, 115, DateTimeKind.Unspecified).AddTicks(1185), "Expedita ipsum vero.", 31, 3, 3 },
                    { 72, new DateTime(2021, 5, 20, 18, 0, 23, 141, DateTimeKind.Unspecified).AddTicks(2902), "Rerum et est qui voluptatem est facilis et sed aut distinctio.", new DateTime(2021, 5, 26, 16, 43, 27, 731, DateTimeKind.Unspecified).AddTicks(3776), "Voluptate sapiente.", 6, 3, 1 },
                    { 1, new DateTime(2020, 4, 25, 0, 45, 38, 153, DateTimeKind.Unspecified).AddTicks(5691), "Sequi placeat quae repudiandae rerum sunt ratione error quisquam rerum dolores praesentium ut.", new DateTime(2020, 5, 20, 17, 33, 43, 264, DateTimeKind.Unspecified).AddTicks(5880), "Molestias qui error nemo neque.", 54, 32, 2 },
                    { 9, new DateTime(2019, 11, 25, 6, 10, 43, 957, DateTimeKind.Unspecified).AddTicks(4021), "Iste velit rerum exercitationem dolorum ut explicabo porro ipsa quia quasi sed labore.", new DateTime(2020, 5, 8, 2, 8, 14, 288, DateTimeKind.Unspecified).AddTicks(4850), "Perferendis suscipit.", 67, 32, 0 },
                    { 12, new DateTime(2020, 2, 12, 7, 47, 2, 690, DateTimeKind.Unspecified).AddTicks(7243), "Ea rerum dolores reprehenderit explicabo odit sed magnam et voluptatem omnis fugit inventore molestias.", new DateTime(2020, 3, 23, 17, 1, 53, 485, DateTimeKind.Unspecified).AddTicks(4270), "Qui est.", 68, 32, 0 },
                    { 36, new DateTime(2019, 8, 11, 12, 17, 4, 365, DateTimeKind.Unspecified).AddTicks(5501), "Illum molestiae et est numquam qui.", new DateTime(2019, 12, 29, 18, 13, 41, 743, DateTimeKind.Unspecified).AddTicks(3315), "Veritatis vel rerum.", 18, 32, 1 },
                    { 64, new DateTime(2020, 4, 14, 13, 40, 59, 264, DateTimeKind.Unspecified).AddTicks(677), "Tempora molestiae sunt doloremque dolores ea nisi et.", new DateTime(2020, 5, 15, 10, 43, 26, 774, DateTimeKind.Unspecified).AddTicks(6950), "Qui aliquam.", 26, 32, 1 },
                    { 80, new DateTime(2020, 3, 28, 22, 48, 21, 813, DateTimeKind.Unspecified).AddTicks(8484), "Et voluptatum consectetur temporibus iure repellat.", new DateTime(2020, 5, 14, 0, 56, 29, 643, DateTimeKind.Unspecified).AddTicks(7947), "Pariatur sit deleniti aut dolor.", 4, 32, 2 },
                    { 93, new DateTime(2019, 1, 2, 1, 11, 24, 282, DateTimeKind.Unspecified).AddTicks(3079), "Dolorem facilis dicta aut reprehenderit deserunt ex consectetur dolor iste fuga in quidem molestiae.", new DateTime(2019, 7, 4, 19, 59, 13, 782, DateTimeKind.Unspecified).AddTicks(886), "Porro et reprehenderit debitis.", 55, 32, 2 },
                    { 111, new DateTime(2019, 6, 15, 19, 30, 6, 419, DateTimeKind.Unspecified).AddTicks(2546), "Itaque voluptatem sit et dignissimos animi sint rerum expedita vel sed quo ea.", new DateTime(2019, 7, 18, 14, 9, 35, 573, DateTimeKind.Unspecified).AddTicks(4069), "Facilis eos beatae repellendus.", 16, 8, 3 },
                    { 125, new DateTime(2019, 6, 25, 15, 25, 44, 415, DateTimeKind.Unspecified).AddTicks(9241), "Non ut nisi consequatur eligendi harum.", new DateTime(2020, 5, 2, 9, 51, 21, 971, DateTimeKind.Unspecified).AddTicks(4982), "Voluptatem necessitatibus tempore vitae maxime.", 25, 15, 3 },
                    { 116, new DateTime(2019, 5, 16, 10, 14, 55, 792, DateTimeKind.Unspecified).AddTicks(2544), "Sequi quidem voluptatum exercitationem aut enim maxime autem.", new DateTime(2020, 8, 12, 17, 23, 24, 838, DateTimeKind.Unspecified).AddTicks(8608), "Quia ratione voluptate alias.", 34, 10, 2 },
                    { 58, new DateTime(2021, 5, 19, 17, 56, 32, 933, DateTimeKind.Unspecified).AddTicks(6912), "Omnis eos dignissimos sunt voluptatem quia officiis commodi autem rerum praesentium est.", new DateTime(2021, 5, 20, 0, 54, 46, 798, DateTimeKind.Unspecified).AddTicks(7427), "Nihil accusamus dolorem ut.", 13, 27, 0 },
                    { 68, new DateTime(2021, 5, 27, 22, 56, 17, 323, DateTimeKind.Unspecified).AddTicks(9651), "Maxime non et libero nulla praesentium ratione ut.", new DateTime(2021, 6, 1, 16, 5, 38, 319, DateTimeKind.Unspecified).AddTicks(8612), "Qui corrupti maiores.", 14, 27, 3 },
                    { 73, new DateTime(2021, 4, 24, 1, 38, 45, 4, DateTimeKind.Unspecified).AddTicks(5131), "Ut sed id quam numquam accusamus suscipit.", new DateTime(2021, 5, 4, 18, 14, 11, 556, DateTimeKind.Unspecified).AddTicks(6226), "Reiciendis consequatur cum ipsum.", 5, 27, 0 },
                    { 133, new DateTime(2021, 4, 27, 6, 18, 14, 751, DateTimeKind.Unspecified).AddTicks(5678), "Pariatur eveniet vel laudantium molestias fugit illum.", new DateTime(2021, 5, 10, 13, 14, 57, 661, DateTimeKind.Unspecified).AddTicks(6540), "Qui magnam quasi nemo.", 55, 27, 0 },
                    { 53, new DateTime(2020, 6, 14, 20, 58, 12, 653, DateTimeKind.Unspecified).AddTicks(1669), "Est magni ratione illum saepe velit consequuntur hic reiciendis quisquam reprehenderit quis.", new DateTime(2020, 9, 21, 7, 27, 51, 171, DateTimeKind.Unspecified).AddTicks(8831), "Asperiores sunt repellat in.", 73, 26, 3 },
                    { 59, new DateTime(2020, 3, 24, 18, 15, 4, 859, DateTimeKind.Unspecified).AddTicks(6857), "Dolorem animi hic ut repudiandae suscipit omnis saepe perferendis itaque.", new DateTime(2020, 6, 1, 7, 52, 22, 467, DateTimeKind.Unspecified).AddTicks(6026), "Sunt enim occaecati omnis cum.", 21, 26, 2 },
                    { 121, new DateTime(2020, 4, 27, 0, 16, 11, 180, DateTimeKind.Unspecified).AddTicks(6383), "Impedit cupiditate animi odit voluptatem animi rem deleniti et voluptas qui.", new DateTime(2020, 7, 26, 16, 46, 46, 151, DateTimeKind.Unspecified).AddTicks(1120), "Omnis facere.", 44, 26, 1 },
                    { 18, new DateTime(2020, 4, 20, 21, 25, 12, 229, DateTimeKind.Unspecified).AddTicks(1237), "Repellendus error aut maiores et consequuntur sunt ut voluptas.", new DateTime(2021, 1, 9, 23, 11, 42, 734, DateTimeKind.Unspecified).AddTicks(2449), "Qui debitis.", 37, 1, 0 },
                    { 50, new DateTime(2020, 1, 20, 18, 10, 26, 641, DateTimeKind.Unspecified).AddTicks(3075), "Dignissimos commodi dolor itaque aut velit blanditiis rem exercitationem.", new DateTime(2020, 2, 2, 6, 6, 41, 105, DateTimeKind.Unspecified).AddTicks(5350), "Ratione inventore fugiat.", 12, 1, 2 },
                    { 90, new DateTime(2020, 10, 14, 1, 22, 11, 503, DateTimeKind.Unspecified).AddTicks(3727), "Sed est mollitia et atque rem voluptatibus.", new DateTime(2020, 11, 8, 6, 34, 13, 304, DateTimeKind.Unspecified).AddTicks(5091), "Iusto nemo.", 7, 1, 2 },
                    { 91, new DateTime(2020, 4, 23, 1, 54, 17, 490, DateTimeKind.Unspecified).AddTicks(5696), "Eos laborum nam doloremque aut aliquam.", new DateTime(2020, 11, 22, 20, 18, 28, 587, DateTimeKind.Unspecified).AddTicks(5430), "Sunt velit quos.", 35, 1, 3 },
                    { 115, new DateTime(2020, 5, 23, 13, 2, 41, 281, DateTimeKind.Unspecified).AddTicks(7136), "In voluptatem reiciendis tenetur ab doloremque.", new DateTime(2020, 8, 24, 4, 58, 44, 413, DateTimeKind.Unspecified).AddTicks(3614), "Ipsam esse voluptatem.", 31, 1, 0 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 41, new DateTime(2020, 4, 28, 3, 59, 0, 130, DateTimeKind.Unspecified).AddTicks(8889), "Aut esse non nisi exercitationem aut qui corrupti.", new DateTime(2020, 7, 18, 5, 15, 31, 2, DateTimeKind.Unspecified).AddTicks(4485), "Eos facere.", 32, 13, 1 },
                    { 76, new DateTime(2020, 6, 16, 3, 43, 50, 301, DateTimeKind.Unspecified).AddTicks(4457), "Qui est sed maiores sunt occaecati.", new DateTime(2020, 6, 30, 20, 22, 39, 143, DateTimeKind.Unspecified).AddTicks(4096), "Dolorem dolore tempore dolorem.", 28, 13, 2 },
                    { 88, new DateTime(2020, 7, 27, 21, 22, 25, 663, DateTimeKind.Unspecified).AddTicks(431), "Sit optio ut provident molestias et eligendi error vel iure praesentium.", new DateTime(2020, 7, 30, 4, 59, 54, 583, DateTimeKind.Unspecified).AddTicks(8498), "Pariatur aut soluta.", 22, 13, 2 },
                    { 48, new DateTime(2021, 4, 26, 11, 27, 31, 674, DateTimeKind.Unspecified).AddTicks(9237), "Velit possimus sit et autem voluptas repellendus earum ex vel quis vero similique nisi.", new DateTime(2021, 5, 7, 8, 53, 57, 764, DateTimeKind.Unspecified).AddTicks(6293), "Vel aspernatur.", 65, 27, 0 },
                    { 63, new DateTime(2020, 5, 3, 21, 5, 34, 841, DateTimeKind.Unspecified).AddTicks(4025), "Distinctio eos maxime omnis neque accusantium dolorum animi magnam vero quam.", new DateTime(2020, 7, 29, 20, 59, 26, 654, DateTimeKind.Unspecified).AddTicks(454), "In incidunt magnam non.", 35, 10, 0 },
                    { 11, new DateTime(2021, 4, 27, 17, 30, 4, 799, DateTimeKind.Unspecified).AddTicks(246), "Autem veritatis rerum quaerat illo cumque consequatur quo temporibus architecto reprehenderit eligendi qui.", new DateTime(2021, 4, 28, 21, 59, 14, 13, DateTimeKind.Unspecified).AddTicks(9709), "Aut autem.", 18, 27, 3 },
                    { 40, new DateTime(2018, 10, 11, 9, 0, 15, 409, DateTimeKind.Unspecified).AddTicks(2769), "Quia maiores nesciunt officia et repellat vitae aut.", new DateTime(2019, 7, 1, 19, 43, 20, 456, DateTimeKind.Unspecified).AddTicks(7544), "Sit nulla amet et ratione.", 17, 22, 3 },
                    { 5, new DateTime(2021, 4, 28, 6, 4, 42, 428, DateTimeKind.Unspecified).AddTicks(576), "Velit consequatur eius libero quia sapiente labore sunt qui eveniet architecto.", new DateTime(2021, 5, 1, 14, 5, 55, 83, DateTimeKind.Unspecified).AddTicks(4718), "Consequuntur eius ex.", 8, 25, 2 },
                    { 8, new DateTime(2021, 3, 17, 0, 47, 48, 975, DateTimeKind.Unspecified).AddTicks(9778), "Quidem laborum rerum dolores quis dicta.", new DateTime(2021, 4, 20, 22, 49, 21, 383, DateTimeKind.Unspecified).AddTicks(6960), "Adipisci sapiente.", 63, 25, 1 },
                    { 14, new DateTime(2021, 3, 19, 8, 30, 40, 752, DateTimeKind.Unspecified).AddTicks(3269), "Qui rem animi quidem non sed quam consequatur id.", new DateTime(2021, 4, 8, 12, 48, 50, 203, DateTimeKind.Unspecified).AddTicks(1138), "Nihil quisquam sed nihil.", 68, 25, 0 },
                    { 22, new DateTime(2021, 4, 20, 8, 13, 44, 277, DateTimeKind.Unspecified).AddTicks(9242), "Illum ratione fugiat occaecati sed at omnis iste hic rerum qui repellendus voluptates ea.", new DateTime(2021, 5, 4, 12, 34, 34, 180, DateTimeKind.Unspecified).AddTicks(9177), "Ut sunt iusto.", 74, 25, 1 },
                    { 95, new DateTime(2021, 3, 9, 0, 22, 3, 440, DateTimeKind.Unspecified).AddTicks(3406), "Sunt quo et nihil asperiores accusantium culpa omnis in et dignissimos.", new DateTime(2021, 4, 13, 13, 12, 32, 128, DateTimeKind.Unspecified).AddTicks(5175), "Laudantium aut ut commodi ad.", 22, 25, 0 },
                    { 127, new DateTime(2021, 4, 11, 1, 50, 18, 595, DateTimeKind.Unspecified).AddTicks(9726), "Blanditiis tempora consectetur voluptatem nihil nobis cumque.", new DateTime(2021, 4, 21, 1, 22, 18, 871, DateTimeKind.Unspecified).AddTicks(5797), "Temporibus eos.", 63, 25, 0 },
                    { 135, new DateTime(2021, 4, 24, 14, 42, 2, 217, DateTimeKind.Unspecified).AddTicks(6443), "Est maiores assumenda aliquam est enim dolores neque quae aut non consectetur aut.", new DateTime(2021, 5, 2, 0, 11, 20, 785, DateTimeKind.Unspecified).AddTicks(9251), "Sit maiores eveniet.", 18, 25, 3 },
                    { 142, new DateTime(2021, 4, 8, 12, 27, 14, 649, DateTimeKind.Unspecified).AddTicks(2894), "Non deleniti cupiditate voluptatem dignissimos consequatur ipsum veritatis ipsa pariatur itaque reprehenderit.", new DateTime(2021, 5, 2, 15, 56, 51, 442, DateTimeKind.Unspecified).AddTicks(4691), "Eveniet laborum expedita.", 46, 25, 2 },
                    { 7, new DateTime(2021, 5, 22, 9, 33, 38, 507, DateTimeKind.Unspecified).AddTicks(3144), "Asperiores perspiciatis sunt fuga nostrum sint amet.", new DateTime(2021, 5, 23, 13, 53, 25, 197, DateTimeKind.Unspecified).AddTicks(1245), "Laborum ex sed.", 20, 35, 2 },
                    { 71, new DateTime(2021, 5, 21, 19, 6, 2, 838, DateTimeKind.Unspecified).AddTicks(7585), "Enim et provident reprehenderit hic nihil occaecati ratione dicta rerum voluptatem nulla.", new DateTime(2021, 5, 22, 15, 46, 17, 772, DateTimeKind.Unspecified).AddTicks(9437), "Laudantium sequi est ut.", 64, 35, 2 },
                    { 107, new DateTime(2021, 5, 22, 9, 47, 14, 301, DateTimeKind.Unspecified).AddTicks(6458), "Voluptatem voluptatem voluptates est ratione odio quas ratione.", new DateTime(2021, 5, 23, 14, 34, 36, 840, DateTimeKind.Unspecified).AddTicks(7086), "Quis qui omnis sint.", 54, 35, 0 },
                    { 126, new DateTime(2021, 5, 22, 18, 55, 30, 244, DateTimeKind.Unspecified).AddTicks(3802), "Nisi sapiente est officiis molestiae culpa.", new DateTime(2021, 5, 23, 0, 30, 49, 502, DateTimeKind.Unspecified).AddTicks(8449), "Optio ut ut.", 29, 35, 0 },
                    { 141, new DateTime(2021, 5, 21, 15, 3, 31, 718, DateTimeKind.Unspecified).AddTicks(4267), "Quos eos veritatis aspernatur facere et et sint.", new DateTime(2021, 5, 22, 9, 14, 38, 635, DateTimeKind.Unspecified).AddTicks(7873), "Quia ut dicta optio.", 13, 35, 2 },
                    { 19, new DateTime(2019, 2, 1, 5, 7, 8, 281, DateTimeKind.Unspecified).AddTicks(4848), "Aliquid qui sed consequatur voluptas optio eligendi cum fugiat impedit consequatur.", new DateTime(2019, 6, 18, 16, 32, 24, 745, DateTimeKind.Unspecified).AddTicks(2808), "Alias tenetur ut.", 66, 22, 0 },
                    { 26, new DateTime(2018, 11, 2, 19, 44, 38, 700, DateTimeKind.Unspecified).AddTicks(8213), "Sequi aliquam ipsam beatae cupiditate aut veritatis iure iusto.", new DateTime(2018, 12, 6, 14, 48, 23, 558, DateTimeKind.Unspecified).AddTicks(3829), "Laudantium eius quisquam.", 30, 22, 0 },
                    { 114, new DateTime(2018, 12, 29, 23, 46, 34, 606, DateTimeKind.Unspecified).AddTicks(7024), "Sequi dolore quod sint ut odit dolorum autem alias sapiente earum.", new DateTime(2019, 7, 13, 22, 27, 51, 973, DateTimeKind.Unspecified).AddTicks(9441), "Repellendus ut.", 71, 22, 1 },
                    { 138, new DateTime(2018, 9, 13, 12, 29, 36, 410, DateTimeKind.Unspecified).AddTicks(2604), "Adipisci eos provident omnis ut ut inventore dolor.", new DateTime(2019, 7, 4, 6, 38, 11, 900, DateTimeKind.Unspecified).AddTicks(4547), "Sunt vero a aliquam eveniet.", 78, 8, 3 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10);
        }
    }
}
