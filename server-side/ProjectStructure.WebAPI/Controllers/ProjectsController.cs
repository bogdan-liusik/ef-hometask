﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> GetAllProjects()
        {
            return Ok(_projectService.GetAllProjects());
        }
        
        [HttpGet("{id:int}")]
        public ActionResult<ProjectDTO> GetProjectById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }
        
        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] ProjectCreateDTO projectCreateDto)
        {
            return Ok(_projectService.CreateProject(projectCreateDto));
        }

        [HttpPut]
        public ActionResult<ProjectDTO> UpdateProject([FromBody] ProjectUpdateDTO projectUpdateDto)
        {
            return Ok(_projectService.UpdateProject(projectUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteProject(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}