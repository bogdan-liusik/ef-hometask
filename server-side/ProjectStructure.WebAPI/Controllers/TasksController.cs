﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        
        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> GetAllTasks()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpGet("{id:int}")]
        public ActionResult<TaskDTO> GetTaskById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }

        [HttpPost]
        public ActionResult<TaskDTO> CreateTask([FromBody] TaskCreateDTO taskCreateDto)
        {
            return Ok(_taskService.CreateTask(taskCreateDto));
        }

        [HttpPut]
        public ActionResult<TaskDTO> UpdateTask([FromBody] TaskUpdateDTO taskUpdateDto)
        {
            return Ok(_taskService.UpdateTask(taskUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteTask(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}