﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> GetAllUsers()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpGet("{id:int}")]
        public ActionResult<UserDTO> GetUserById(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser([FromBody] UserCreateDTO userCreateDto)
        {
            return Ok(_userService.CreateUser(userCreateDto));
        }

        [HttpPut]
        public ActionResult<UserDTO> UpdateUser([FromBody] UserUpdateDTO userUpdateDto)
        {
            return Ok(_userService.UpdateUser(userUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteUser(int id)
        {
            _userService.DeleteUser(id);
            return NoContent();
        }
    }
}