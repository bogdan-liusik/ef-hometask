﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        
        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> GetAllTeams()
        {
            return Ok(_teamService.GetAllTeams());
        }

        [HttpGet("{id:int}")]
        public ActionResult<TeamDTO> GetTeamById(int id)
        {
            return Ok(_teamService.GetTeamById(id));
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam([FromBody] TeamCreateDTO teamCreateDto)
        {
            return Ok(_teamService.CreateTeam(teamCreateDto));
        }

        [HttpPut]
        public ActionResult<TeamDTO> UpdateTeam([FromBody] TeamUpdateDTO teamUpdateDto)
        {
            return Ok(_teamService.UpdateTeam(teamUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteTeam(int id)
        {
            _teamService.DeleteTeam(id);
            return NoContent();
        }
    }
}