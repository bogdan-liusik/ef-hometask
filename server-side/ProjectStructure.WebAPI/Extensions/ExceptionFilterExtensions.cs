﻿using System;
using System.Net;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.WebAPI.Enums;

namespace ProjectStructure.WebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static (HttpStatusCode statusCode, ErrorCode errorCode) ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException _ => (HttpStatusCode.NotFound, ErrorCode.NotFound),
                SystemException _ => (HttpStatusCode.InternalServerError, ErrorCode.InternalServerError)
            };
        }
    }
}