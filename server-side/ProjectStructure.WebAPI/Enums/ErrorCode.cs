﻿namespace ProjectStructure.WebAPI.Enums
{
    public enum ErrorCode
    {
        NotFound = 404,
        InternalServerError = 500
    }
}