﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IMapper _mapper;
        private protected readonly ProjectsContext _context;

        public BaseService(ProjectsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        protected ICollection<TEntity> GetAllEntities<TEntity>() where TEntity: BaseEntity
        {
            return _context.Set<TEntity>().ToList();
        }

        protected TEntity GetEntityById<TEntity>(int entityId) where TEntity: BaseEntity
        {
            var entity = _context.Set<TEntity>().SingleOrDefault(e => e.Id == entityId);

            if (entity == null)
            {
                throw new NotFoundException(typeof(TEntity).ToString(), entityId);
            }

            return entity;
        }
    }
}