﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public ICollection<TeamDTO> GetAllTeams()
        {
            return GetAllEntities<Team>()
                .Select(team => _mapper.Map<TeamDTO>(team))
                .ToList();
        }

        public TeamDTO GetTeamById(int id)
        {
            var teamEntity = GetEntityById<Team>(id);
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public TeamDTO CreateTeam(TeamCreateDTO teamCreateDto)
        {
            var teamEntity = _mapper.Map<Team>(teamCreateDto);
            var createdTeam = _context.Teams.Add(teamEntity).Entity;
            _context.SaveChanges();
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public TeamDTO UpdateTeam(TeamUpdateDTO teamUpdateDto)
        {
            var teamEntity = GetEntityById<Team>(teamUpdateDto.Id);

            teamEntity.Name = teamUpdateDto.Name;
            
            _context.SaveChanges();
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void DeleteTeam(int id)
        {
            var entityTeam = GetEntityById<Team>(id);
            _context.Teams.Remove(entityTeam);
            _context.SaveChanges();
        }
    }
}